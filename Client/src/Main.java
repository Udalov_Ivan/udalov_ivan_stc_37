import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // сканнер для считывания сообщений с консоли
        Scanner scanner = new Scanner(System.in);
        // отдельный поток, который получает и отправляет сообщения серверу
        ClientReceiverThread clientReceiverThread = new ClientReceiverThread("localhost", 7777);
        // запускаем отдельный поток
        clientReceiverThread.start();

        // бесконечный main-поток
        while (true) {
            // считываем сообщение от клиента
            String message = scanner.nextLine();
            // отправляем его на сервер
            clientReceiverThread.sendMessage(message);
        }

    }
}
