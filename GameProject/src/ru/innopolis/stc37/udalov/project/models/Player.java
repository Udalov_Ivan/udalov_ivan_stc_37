package ru.innopolis.stc37.udalov.project.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class Player {
    private Long id;
    private String playerIpAddress;
    private String playerName;
    private Integer maxPoint;
    private Integer winCount;
    private Integer loseCount;

    public Player() {

    }

    public Player(Long id, String playerIpAddress, String playerName, Integer maxPoint, Integer winCount, Integer loseCount) {
        this.id = id;
        this.playerIpAddress = playerIpAddress;
        this.playerName = playerName;
        this.maxPoint = maxPoint;
        this.winCount = winCount;
        this.loseCount = loseCount;
    }

    public Player(String playerIpAddress, String playerName, Integer maxPoint, Integer winCount, Integer loseCount) {
        this.playerIpAddress = playerIpAddress;
        this.playerName = playerName;
        this.maxPoint = maxPoint;
        this.winCount = winCount;
        this.loseCount = loseCount;
    }

    public String getPlayerIpAddress() {
        return playerIpAddress;
    }

    public void setPlayerIpAddress(String playerIpAddress) {
        this.playerIpAddress = playerIpAddress;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(Integer maxPoint) {
        this.maxPoint = maxPoint;
    }

    public Integer getWinCount() {
        return winCount;
    }

    public void setWinCount(Integer winCount) {
        this.winCount = winCount;
    }

    public Integer getLoseCount() {
        return loseCount;
    }

    public void setLoseCount(Integer loseCount) {
        this.loseCount = loseCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(playerIpAddress, player.playerIpAddress) &&
                Objects.equals(playerName, player.playerName) &&
                Objects.equals(maxPoint, player.maxPoint) &&
                Objects.equals(winCount, player.winCount) &&
                Objects.equals(loseCount, player.loseCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerIpAddress, playerName, maxPoint, winCount, loseCount);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Player.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("playerIpAddress='" + playerIpAddress + "'")
                .add("playerName='" + playerName + "'")
                .add("maxPoint=" + maxPoint)
                .add("winCount=" + winCount)
                .add("loseCount=" + loseCount)
                .toString();
    }

    public static class Builder {

        private Player player;

        public Builder() {
            player = new Player();
        }

        public Builder id(Long id) {
            player.id = id;
            return this;
        }

        public Builder playerIpAddress(String playerIpAddress) {
            player.playerIpAddress = playerIpAddress;
            return this;
        }

        public Builder playerName(String playerName) {
            player.playerName = playerName;
            return this;
        }

        public Builder maxPoint(Integer maxPoint) {
            player.maxPoint = maxPoint;
            return this;
        }

        public Builder winCount(Integer winCount) {
            player.winCount = winCount;
            return this;
        }

        public Builder loseCount(Integer loseCount) {
            player.loseCount = loseCount;
            return this;
        }

        public Player build() {
            return player;
        }

    }
}
