package ru.innopolis.stc37.udalov.project.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class Game {

    private Long id;
    private LocalDateTime dateOfGame;
    private Player playerOne;
    private Player playerTwo;
    private Integer playerOneShootsCounters;
    private Integer playerTwoShootsCounters;
    private Long timeOfGame;

    public Game(){

    }

    public Game(Long id, LocalDateTime dateOfGame, Player playerOne, Player playerTwo, Integer playerOneShootsCounters, Integer playerTwoShootsCounters, Long timeOfGame) {
        this.id = id;
        this.dateOfGame = dateOfGame;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.playerOneShootsCounters = playerOneShootsCounters;
        this.playerTwoShootsCounters = playerTwoShootsCounters;
        this.timeOfGame = timeOfGame;
    }

    public Game(LocalDateTime dateOfGame, Player playerOne, Player playerTwo, Integer playerOneShootsCounters, Integer playerTwoShootsCounters, Long timeOfGame) {
        this.dateOfGame = dateOfGame;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.playerOneShootsCounters = playerOneShootsCounters;
        this.playerTwoShootsCounters = playerTwoShootsCounters;
        this.timeOfGame = timeOfGame;
    }

    public LocalDateTime getDateOfGame() {
        return dateOfGame;
    }

    public void setDateOfGame(LocalDateTime dateOfGame) {
        this.dateOfGame = dateOfGame;
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(Player playerOne) {
        this.playerOne = playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(Player playerTwo) {
        this.playerTwo = playerTwo;
    }

    public Integer getPlayerOneShootsCounters() {
        return playerOneShootsCounters;
    }

    public void setPlayerOneShootsCounters(Integer playerOneShootsCounters) {
        this.playerOneShootsCounters = playerOneShootsCounters;
    }

    public Integer getPlayerTwoShootsCounters() {
        return playerTwoShootsCounters;
    }

    public void setPlayerTwoShootsCounters(Integer playerTwoShootsCounters) {
        this.playerTwoShootsCounters = playerTwoShootsCounters;
    }

    public Long getTimeOfGame() {
        return timeOfGame;
    }

    public void setTimeOfGame(Long timeOfGame) {
        this.timeOfGame = timeOfGame;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(dateOfGame, game.dateOfGame) &&
                Objects.equals(playerOne, game.playerOne) &&
                Objects.equals(playerTwo, game.playerTwo) &&
                Objects.equals(playerOneShootsCounters, game.playerOneShootsCounters) &&
                Objects.equals(playerTwoShootsCounters, game.playerTwoShootsCounters) &&
                Objects.equals(timeOfGame, game.timeOfGame);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateOfGame, playerOne, playerTwo, playerOneShootsCounters, playerTwoShootsCounters, timeOfGame);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Game.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("dateOfGame=" + dateOfGame)
                .add("playerOne=" + playerOne.getPlayerName())
                .add("playerTwo=" + playerTwo.getPlayerName())
                .add("playerOneShootsCounters=" + playerOneShootsCounters)
                .add("playerTwoShootsCounters=" + playerTwoShootsCounters)
                .add("timeOfGame=" + timeOfGame)
                .toString();
    }

    public static class Builder {

        private Game game;

        public Builder() {
            game = new Game();
        }

        public Builder id(Long id) {
            game.id = id;
            return this;
        }

        public Builder dateOfGame(LocalDateTime dateOfGame) {
            game.dateOfGame = dateOfGame;
            return this;
        }

        public Builder playerOne(Player playerOne) {
            game.playerOne = playerOne;
            return this;
        }

        public Builder playerTwo(Player playerTwo) {
            game.playerTwo = playerTwo;
            return this;
        }

        public Builder playerOneShootsCounters(Integer playerOneShootsCounters) {
            game.playerOneShootsCounters = playerOneShootsCounters;
            return this;
        }

        public Builder playerTwoShootsCounters(Integer playerTwoShootsCounters) {
            game.playerTwoShootsCounters = playerTwoShootsCounters;
            return this;
        }

        public Builder timeOfGame(Long timeOfGame) {
            game.timeOfGame = timeOfGame;
            return this;
        }

        public Game build() {
            return game;
        }

    }
}
