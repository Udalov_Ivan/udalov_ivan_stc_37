package ru.innopolis.stc37.udalov.project.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class Shot {
    private Long id;
    private LocalDateTime timeToShoot;
    private Game game;
    private Player playerWhoShoot;
    private Player playerWhoIsTheTarget;

    public Shot(){

    }

    public Shot(Long id, LocalDateTime timeToShoot, Game game, Player playerWhoShoot, Player playerWhoIsTheTarget) {
        this.id = id;
        this.timeToShoot = timeToShoot;
        this.game = game;
        this.playerWhoShoot = playerWhoShoot;
        this.playerWhoIsTheTarget = playerWhoIsTheTarget;
    }

    public Shot(LocalDateTime timeToShoot, Game game, Player thePlayerWhoShoot, Player thePlayerWhoIsTheTarget) {
        this.timeToShoot = timeToShoot;
        this.game = game;
        this.playerWhoShoot = thePlayerWhoShoot;
        this.playerWhoIsTheTarget = thePlayerWhoIsTheTarget;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTimeToShoot() {
        return timeToShoot;
    }

    public void setTimeToShoot(LocalDateTime timeToShoot) {
        this.timeToShoot = timeToShoot;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getPlayerWhoShoot() {
        return playerWhoShoot;
    }

    public void setPlayerWhoShoot(Player playerWhoShoot) {
        this.playerWhoShoot = playerWhoShoot;
    }

    public Player getPlayerWhoIsTheTarget() {
        return playerWhoIsTheTarget;
    }

    public void setPlayerWhoIsTheTarget(Player playerWhoIsTheTarget) {
        this.playerWhoIsTheTarget = playerWhoIsTheTarget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(timeToShoot, shot.timeToShoot) &&
                Objects.equals(game, shot.game) &&
                Objects.equals(playerWhoShoot, shot.playerWhoShoot) &&
                Objects.equals(playerWhoIsTheTarget, shot.playerWhoIsTheTarget);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeToShoot, game, playerWhoShoot, playerWhoIsTheTarget);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Shot.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("timeToShoot=" + timeToShoot)
                .add("game=" + game.getId())
                .add("thePlayerWhoShoot=" + playerWhoShoot.getPlayerName())
                .add("thePlayerWhoIsTheTarget=" + playerWhoIsTheTarget.getPlayerName())
                .toString();
    }

    public static class Builder {

        private Shot shot;

        public Builder() {
            shot = new Shot();
        }

        public Builder id(Long id) {
            shot.id = id;
            return this;
        }

        public Builder timeToShoot(LocalDateTime timeToShoot) {
            shot.timeToShoot = timeToShoot;
            return this;
        }

        public Builder game(Game game) {
            shot.game = game;
            return this;
        }

        public Builder playerWhoShoot(Player PlayerWhoShoot) {
            shot.playerWhoShoot = PlayerWhoShoot;
            return this;
        }

        public Builder playerWhoIsTheTarget(Player PlayerWhoIsTheTarget) {
            shot.playerWhoIsTheTarget = PlayerWhoIsTheTarget;
            return this;
        }

        public Shot build() {
            return shot;
        }

    }
}
