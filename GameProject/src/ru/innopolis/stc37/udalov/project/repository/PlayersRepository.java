package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Player;

public interface PlayersRepository {
    //поиск игрока по имени
    Player findByPlayerName(String playerName);

    //сохранение игрока
    void save(Player player);

    //обновление информациии о игроке в хранилище
    void update(Player player);
}
