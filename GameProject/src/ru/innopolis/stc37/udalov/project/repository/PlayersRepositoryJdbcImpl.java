package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

import static ru.innopolis.stc37.udalov.project.utils.JdbcUtil.closeJdbcObjects;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {


    //language=SQL
    private static final String SQL_INSERT_PLAYER = "INSERT INTO " +
            "player (player_ip_address, player_name, max_point, win_count, lose_count) VALUES (?, ?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NAME = "SELECT player_name, * FROM player WHERE player_name = ?";


    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_ID = "UPDATE player SET " +
            "player_ip_address = ?, player_name = ?, max_point = ?, win_count = ?, lose_count = ? WHERE id = ?";

    //функция которая преобразует строку row из базы данных в обьект Player
    static private final RowMapper<Player> playerRowMapper = row -> new Player.Builder()
            .id(row.getLong("id"))
            .playerIpAddress(row.getString("player_ip_address"))
            .playerName(row.getString("player_name"))
            .maxPoint(row.getInt("max_point"))
            .winCount(row.getInt("win_count"))
            .loseCount(row.getInt("lose_count"))
            .build();

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByPlayerName(String playerName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Player player = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NAME);
            //кладем playerName в данный statement
            statement.setString(1, playerName);

            rows = statement.executeQuery();

            if (rows.next()) {
                player = playerRowMapper.mapRow(rows);
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }

        return player;
    }

    @Override
    public void save(Player player) {
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            //RETURN_GENERATED_KEYS - запрос должен вернуть ключи(id), которые сгенерировала БД
            statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, player.getPlayerIpAddress());
            statement.setString(2, player.getPlayerName());
            statement.setInt(3, player.getMaxPoint());
            statement.setInt(4, player.getWinCount());
            statement.setInt(5, player.getLoseCount());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't insert player!");
            }

            //создание id для нового игрока
            //ключи которые база сгенерировала сама для этого запроса
            generatedId = statement.getGeneratedKeys();
            //проверяем сгенерировала ли база что либо?
            if (generatedId.next()) {
                //получаем ключ, который сгенерировала база для текущего запроса
                player.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
            } else {
                throw new SQLException("Can't retrieve id!"); //если id не сгенерирован
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public void update(Player player) {
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            //для текущего запроса
            statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_ID);

            statement.setString(1, player.getPlayerIpAddress());
            statement.setString(2, player.getPlayerName());
            statement.setInt(3, player.getMaxPoint());
            statement.setInt(4, player.getWinCount());
            statement.setInt(5, player.getLoseCount());
            statement.setLong(6, player.getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't update player!");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }
}
