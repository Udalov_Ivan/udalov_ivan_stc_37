package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.models.Shot;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

import static ru.innopolis.stc37.udalov.project.utils.JdbcUtil.closeJdbcObjects;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT_SHOT = "INSERT INTO " +
            "shot (time_to_shoot, game, player_who_shoot, player_who_target) VALUES (?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_UPDATE_SHOT_BY_ID = "UPDATE shot SET " +
            "time_to_shoot = ?, game = ?, player_who_shoot = ?, player_who_target = ? WHERE id = ?";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "SELECT * FROM shot WHERE id = ?";

    static private final RowMapper<Shot> shotRowMapper = row -> new Shot.Builder()
            .id(row.getLong("id"))
            .timeToShoot(LocalDateTime.parse(row.getString("time_to_shoot")))
            .game(new Game.Builder()
                    .id(row.getLong("game"))
                    .build())
            .playerWhoShoot(new Player.Builder()
                    .id(row.getLong("player_who_shoot"))
                    .build())
            .playerWhoIsTheTarget(new Player.Builder()
                    .id(row.getLong("player_who_target"))
                    .build())
            .build();

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            //RETURN_GENERATED_KEYS - запрос должен вернуть ключи(id), которые сгенерировала БД
            statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, shot.getTimeToShoot().toString());
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getPlayerWhoShoot().getId());
            statement.setLong(4, shot.getPlayerWhoIsTheTarget().getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            //если affectedRows не равен 1, то не получилось добавить выстрел
            if (affectedRows != 1) {
                throw new SQLException("Can't insert shoot!");
            }

            //создание id для нового выстрела
            //ключи которые база сгенерировала сама для этого запроса
            generatedId = statement.getGeneratedKeys();
            //проверяем сгенерировала ли база что либо?
            if (generatedId.next()) {
                //получаем ключ, который сгенерировала база для текущего запроса
                shot.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
            } else {
                throw new SQLException("Can't retrieve id!"); //если id не сгенерирован
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public Shot findById(Long shotId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Shot shot = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID);
            //кладем shotId в данный statement
            statement.setLong(1, shotId);

            rows = statement.executeQuery();

            if (rows.next()) {
                shot = shotRowMapper.mapRow(rows);
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }

        return shot;
    }

    @Override
    public void update(Shot shot) {
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            //для текущего запроса
            statement = connection.prepareStatement(SQL_UPDATE_SHOT_BY_ID);

            statement.setString(1, shot.getTimeToShoot().toString());
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getPlayerWhoShoot().getId());
            statement.setLong(4, shot.getPlayerWhoIsTheTarget().getId());
            statement.setLong(5, shot.getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            //если affectedRows не равен 1, то не получилось добавить выстрел
            if (affectedRows != 1) {
                throw new SQLException("Can't update shot!");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }
}
