package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;


import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

import static ru.innopolis.stc37.udalov.project.utils.JdbcUtil.closeJdbcObjects;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "INSERT INTO " +
            "game (date_of_game, player_one, player_two, player_one_shot_count, player_two_shot_count, time_of_game) " +
            "VALUES (?, ?, ?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "SELECT * FROM game WHERE id = ?";

    //language=SQL
    private static final String SQL_UPDATE_GAME_BY_ID = "UPDATE game SET " +
            "date_of_game = ?, player_one = ?, player_two = ?, player_one_shot_count = ?, player_two_shot_count = ?, time_of_game = ?" +
            " WHERE id = ?";


    static private final RowMapper<Game> gameRowMapper = row -> new Game.Builder()
            .id(row.getLong("id"))
            .dateOfGame(LocalDateTime.parse(row.getString("date_of_game")))
            .playerOne(new Player.Builder()
                    .id(row.getLong("player_one"))
                    .build())
            .playerTwo(new Player.Builder()
                    .id(row.getLong("player_two"))
                    .build())
            .playerOneShootsCounters(row.getInt("player_one_shot_count"))
            .playerTwoShootsCounters(row.getInt("player_two_shot_count"))
            .timeOfGame(row.getLong("time_of_game"))
            .build();

    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            //RETURN_GENERATED_KEYS - запрос должен вернуть ключи(id), которые сгенерировала БД
            statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, game.getDateOfGame().toString());
            statement.setLong(2, game.getPlayerOne().getId());
            statement.setLong(3, game.getPlayerTwo().getId());
            statement.setInt(4, game.getPlayerOneShootsCounters());
            statement.setInt(5, game.getPlayerTwoShootsCounters());
            statement.setLong(6, game.getTimeOfGame());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't insert game!");
            }

            //создание id для нового игрока
            //ключи которые база сгенерировала сама для этого запроса
            generatedId = statement.getGeneratedKeys();
            //проверяем сгенерировала ли база что либо?
            if (generatedId.next()) {
                //получаем ключ, который сгенерировала база для текущего запроса
                game.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
            } else {
                throw new SQLException("Can't retrieve id!"); //если id не сгенерирован
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public Game findById(Long gameId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Game game = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID);
            //кладем gameId в данный statement
            statement.setLong(1, gameId);

            rows = statement.executeQuery();

            if (rows.next()) {
                game = gameRowMapper.mapRow(rows);
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }

        return game;
    }

    @Override
    public void update(Game game) {
        PreparedStatement statement = null;
        ResultSet generatedId = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            //для текущего запроса
            statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID);

            statement.setString(1, game.getDateOfGame().toString());
            statement.setLong(2, game.getPlayerOne().getId());
            statement.setLong(3, game.getPlayerTwo().getId());
            statement.setInt(4, game.getPlayerOneShootsCounters());
            statement.setInt(5, game.getPlayerTwoShootsCounters());
            statement.setLong(6, game.getTimeOfGame());
            statement.setLong(7, game.getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't update game!");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }
}
