package ru.innopolis.stc37.udalov.project.services;

import ru.innopolis.stc37.udalov.project.dto.StatisticDto;
import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.models.Shot;
import ru.innopolis.stc37.udalov.project.repository.GamesRepository;
import ru.innopolis.stc37.udalov.project.repository.PlayersRepository;
import ru.innopolis.stc37.udalov.project.repository.ShotsRepository;
import java.time.LocalDateTime;

//бизнес-логика
public class GameServiceImpl implements GameService {

    private final PlayersRepository playersRepository;
    private final GamesRepository gamesRepository;
    private final ShotsRepository shotsRepository;

    //конструктор
    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String ipOne, String ipTwo, String playerOneName, String playerTwoName) {
        //выполняем метод checkIfPlayersExists для получения информации об игроках и их сохранение
        Player playerOne = checkIfPlayersExists(ipOne, playerOneName);
        Player playerTwo = checkIfPlayersExists(ipTwo, playerTwoName);

        //создание новой игры
        Game game = new Game.Builder()
                .dateOfGame(LocalDateTime.now())
                .playerOne(playerOne)
                .playerTwo(playerTwo)
                .playerOneShootsCounters(0)
                .playerTwoShootsCounters(0)
                .timeOfGame(0L)
                .build();

        //сохранение в репозитории игры
        gamesRepository.save(game);
        // возвращаем идентификактор игры
        return game.getId();
    }

    private Player checkIfPlayersExists(String ip, String playerName) {
        // получение информации об обоих игроках
        Player player = playersRepository.findByPlayerName(playerName);
        // если нет таких игроков
        if (player == null) {
            // создать игрока
            player = new Player(ip, playerName, 0, 0, 0);
            //сохраняем игрока
            playersRepository.save(player);
        } else {
            //если такой игрок в системе уже был, обновляем ip-адресс и информацию о нем
            player.setPlayerIpAddress(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String playerWhoShoot, String playerWhoTarget) {
        //отсчет времени
        long start = System.currentTimeMillis();

        //получаем того, кто стрелял из репозитория
        Player shooterPlayer = playersRepository.findByPlayerName(playerWhoShoot);
        //получаем того, в кого стреляли из репозитория
        Player targetPlayer = playersRepository.findByPlayerName(playerWhoTarget);
        //получаем игру
        Game game = gamesRepository.findById(gameId);
        //создаем выстрел
        Shot shot = new Shot.Builder()
                .timeToShoot(LocalDateTime.now())
                .game(game)
                .playerWhoShoot(shooterPlayer)
                .playerWhoIsTheTarget(targetPlayer)
                .build();

        game.setDateOfGame(LocalDateTime.now());

        //учитывание попадагий и увеличении очков
        shooterPlayer.setMaxPoint(shooterPlayer.getMaxPoint() + 1);

        //кто из игроков сделал выстрел
        //если стрелял первый игрок
        if (game.getPlayerOne().getPlayerName().equals(playerWhoShoot)) {
            //сохроняем информацию о выстреле в игре
            game.setPlayerOneShootsCounters(game.getPlayerOneShootsCounters() + 1);
        }
        //если стрелял второй игрок
        if (game.getPlayerTwo().getPlayerName().equals(playerWhoShoot)) {
            //сохроняем информацию о выстреле в игре
            game.setPlayerTwoShootsCounters(game.getPlayerTwoShootsCounters() + 1);
        }

        //окончание отсчета времени
        long finish = System.currentTimeMillis();
        //сколько времени прошло
        game.setTimeOfGame(finish - start);

        //обнавляем данные среляющего игрока в репозитории
        playersRepository.update(shooterPlayer);
        //обновление данных игры в репозитории
        gamesRepository.update(game);
        //сохранение выстрела в репозиторий
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId, String playerOne, String playerTwo) {
        StatisticDto statisticDto = new StatisticDto();
        final Game game = gamesRepository.findById(gameId);
        final Player playerFirst = playersRepository.findByPlayerName(playerOne);
        final Player playerSecond = playersRepository.findByPlayerName(playerTwo);

        //имя игрока
        final String playerNameOne = game.getPlayerOne().getPlayerName();
        final String playerNameTwo = game.getPlayerTwo().getPlayerName();
        //попадания
        final int hitPlayerOne = game.getPlayerOneShootsCounters();
        final int hitPlayerTwo = game.getPlayerTwoShootsCounters();
        //максимальное количество очков
        final int maxPointPlayerOne = playerFirst.getMaxPoint();
        final int maxPointPlayerTwo = playerSecond.getMaxPoint();
        //количество побед
        int maxWinPlayerOne = playerFirst.getWinCount();
        int maxWinPlayerTwo = playerSecond.getWinCount();
        //количество поражений
        int loseCountPlayerOne = playerFirst.getLoseCount();
        int loseCountPlayerTwo = playerSecond.getLoseCount();

        //вывод информации при завершение игры
        finishGameLogic(statisticDto, game, playerFirst,
                playerSecond, playerNameOne, playerNameTwo,
                hitPlayerOne, hitPlayerTwo, maxPointPlayerOne, maxPointPlayerTwo,
                maxWinPlayerOne, maxWinPlayerTwo, loseCountPlayerOne, loseCountPlayerTwo);

        //сохраняем изменения
        playersRepository.update(playerFirst);
        playersRepository.update(playerSecond);
        gamesRepository.update(game);

        return statisticDto;
    }

    private void finishGameLogic(StatisticDto statisticDto, Game game, Player playerFirst,
                                 Player playerSecond, String playerNameOne, String playerNameTwo,
                                 int hitPlayerOne, int hitPlayerTwo, int maxPointPlayerOne, int maxPointPlayerTwo,
                                 int maxWinPlayerOne, int maxWinPlayerTwo, int loseCountPlayerOne, int loseCountPlayerTwo) {
        //идентификатор игры
        System.out.println("\nGame id is: " + game.getId() +
                "; Date of Game: " + game.getDateOfGame() +
                ";");

        //еслы выиграл первый
        if (hitPlayerOne > hitPlayerTwo) {
            playerFirst.setWinCount(playerFirst.getWinCount() + 1);
            playerSecond.setLoseCount(playerSecond.getLoseCount() + 1);

            playersInfo(playerNameOne, playerNameTwo, hitPlayerOne, hitPlayerTwo, maxPointPlayerOne,
                    maxPointPlayerTwo, maxWinPlayerOne, maxWinPlayerTwo, loseCountPlayerOne, loseCountPlayerTwo);
            System.out.println("Winner is: " + playerNameOne + ";");
        }
        //если выиграл второй
        if (hitPlayerOne < hitPlayerTwo) {
            playerSecond.setWinCount(playerSecond.getWinCount() + 1);
            playerFirst.setLoseCount(playerFirst.getLoseCount() + 1);

            playersInfo(playerNameOne, playerNameTwo, hitPlayerOne, hitPlayerTwo, maxPointPlayerOne,
                    maxPointPlayerTwo, maxWinPlayerOne, maxWinPlayerTwo, loseCountPlayerOne, loseCountPlayerTwo);
            System.out.println("Winner is: " + playerNameTwo + ";");
        //если ничья
        } else if (hitPlayerOne == hitPlayerTwo) {
            playerFirst.setWinCount(playerFirst.getWinCount());
            playerSecond.setWinCount(playerSecond.getWinCount());
            playerSecond.setLoseCount(playerSecond.getLoseCount());
            playerFirst.setLoseCount(playerFirst.getLoseCount());

            playersInfo(playerNameOne, playerNameTwo, hitPlayerOne, hitPlayerTwo, maxPointPlayerOne,
                    maxPointPlayerTwo, maxWinPlayerOne, maxWinPlayerTwo, loseCountPlayerOne, loseCountPlayerTwo);
            System.out.println("Nobody won because it’s a Draw!;");
        }

        //TODO: переписать время затраченное на игру (сейчас считает время в методе startGame)
        statisticDto.setTimeOfGame(game.getTimeOfGame());
    }

    private void playersInfo(String playerNameOne, String playerNameTwo, int hitPlayerOne, int hitPlayerTwo, int maxPointPlayerOne,
                             int maxPointPlayerTwo, int maxWinPlayerOne, int maxWinPlayerTwo, int loseCountPlayerOne, int loseCountPlayerTwo) {
        System.out.println("Player One: " + playerNameOne +
                "; hit = " + hitPlayerOne +
                "; Max Point = " + maxPointPlayerOne +
                "; Win Count = " + maxWinPlayerOne +
                "; Lose Count = " + loseCountPlayerOne +
                ";");
        System.out.println("Player Two: " + playerNameTwo +
                "; hit = " + hitPlayerTwo +
                "; Max Point = " + maxPointPlayerTwo +
                "; Win Count = " + maxWinPlayerTwo +
                "; Lose Count = " + loseCountPlayerTwo +
                ";");
    }

}
