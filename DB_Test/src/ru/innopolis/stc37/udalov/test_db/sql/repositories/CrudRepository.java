package ru.innopolis.stc37.udalov.test_db.sql.repositories;

import java.util.List;

//CRUD - Create(save) Read(findById, findAll) Update(update), Delete(remove)
//общий интерфейс для сущностей(обьектов)
public interface CrudRepository<T> {

    List<T> findAll();
    void save(T model);
    void update(T model);
    void remove(long id);
    T findById(long id);



}
