package ru.innopolis.stc37.udalov.test_db.sql.app;

import ru.innopolis.stc37.udalov.test_db.sql.models.Driver;
import ru.innopolis.stc37.udalov.test_db.sql.repositories.CarsRepository;
import ru.innopolis.stc37.udalov.test_db.sql.repositories.CarsRepositoryJdbcImpl;
import ru.innopolis.stc37.udalov.test_db.sql.repositories.DriversRepository;
import ru.innopolis.stc37.udalov.test_db.sql.repositories.DriversRepositoryJdbcImpl;
import ru.innopolis.stc37.udalov.test_db.sql.utils.CustomDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

public class Main2 {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "post116";

    //language=SQL
    private static final String SQL_SELECT_DRIVERS = "SELECT * FROM driver"; //выбрать таблицу driver

    public static void main(String[] args){


//  Если хотим воспользоваться подключением через connection, а не datasource
//        Connection connection;
//
//        try {
//            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
//        } catch (SQLException throwables) {
//            throw new IllegalArgumentException(throwables);
//        }

        //обьект занимающийся подключениями реализован в методе getConnection в классе CustomDataSource
        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);

        DriversRepository driversRepository = new DriversRepositoryJdbcImpl(dataSource);
        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);

        //Пагинация
        //выводим 2 страницу размером 4 из таблицы Car (страницы начинаются с 0 страницы)
        System.out.println(carsRepository.findAll(1, 4));

        Driver driver = new Driver("CreateDriver", "DriverCreated", 30, 1.95);
        //сохроняем водителя, но после сохранения не знаем какой у него id,
        //но делаем так, чтобы узнать его
        //driversRepository.save(driver);
        System.out.println(driver);

        //находим всех
        //List<Driver> drivers = driversRepository.findAll();
        //находим всех владельцев с машинами
        List<Driver> drivers = driversRepository.findAllWithCars();

        System.out.println(drivers);

        //находим владельца по id
        //System.out.println(driversRepository.findById(6L));

        //update
        Driver driver6L = driversRepository.findById(6L);
        driver6L.setFirstName("Maximus");
        driversRepository.update(driver6L);


//  Дял подключения через connection
//        try {
//            connection.close();
//        } catch (SQLException throwables) {
//            throw new IllegalArgumentException(throwables);
//        }
    }
}