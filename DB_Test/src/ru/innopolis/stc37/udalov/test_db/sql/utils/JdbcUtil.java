package ru.innopolis.stc37.udalov.test_db.sql.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcUtil {
    //служебный метод закрывающий JDBC обьекты
    public static void closeJdbcObjects(Connection connection, Statement statement, ResultSet rows) {
        //гарантирует исполнение кода
        //в какомпорядке получали - в обратном закрываем
        if (rows != null) {
            try {
                rows.close();
            } catch (SQLException ignore) {
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
            }
        }

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ignore){
            }
        }
    }
}
