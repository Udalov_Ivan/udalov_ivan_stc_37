package ru.innopolis.stc37.udalov.test_db.sql.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "post116";

    //language=SQL
    private static final String SQL_SELECT_DRIVERS = "SELECT * FROM driver"; //выбрать таблицу driver

    public static void main(String[] args) throws Exception{

        //подключение к базе данных
        Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);

        //отправить запрос в БД
        //statement это обьект который умеет выполнять запросы
        Statement statement = connection.createStatement();
        //выполняем запрос
        //получили ResultSet - обьект итератор по результирующим строкам
        ResultSet rows = statement.executeQuery(SQL_SELECT_DRIVERS); //для таблицы driver

        while(rows.next()){
            //получить все id, имена и рост из таблицы driver
            System.out.println(rows.getLong("id") + " " +
                    rows.getString("first_name") + " " +
                    rows.getDouble("height"));
        }

        //добавление в базу данных

        System.out.println("Добавление нового водителя (Введите имя): ");

        Scanner scanner = new Scanner(System.in);
        String firstName = scanner.nextLine();

        //language=SQL
        String insertDriver = "INSERT INTO driver(first_name) VALUES ('" + firstName + "')"; //отправляем запрос на добавление
        //печатаем информацию о выполнении запроса (INSERT INTO driver(first_name) VALUES ('имя нового водителя'))
        System.out.println(insertDriver);
        //выполняем запрос
        statement.executeUpdate(insertDriver);

    }
}
