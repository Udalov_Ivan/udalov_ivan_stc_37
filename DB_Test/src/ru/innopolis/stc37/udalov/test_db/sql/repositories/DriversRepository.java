package ru.innopolis.stc37.udalov.test_db.sql.repositories;

import ru.innopolis.stc37.udalov.test_db.sql.models.Driver;

import java.util.List;

//уточняющий репозиторий
public interface DriversRepository extends CrudRepository<Driver>{
    List<Driver> findAllByAge(int age);
    List<Driver> findAllWithCars(); //получить всех водителей с машинами одним запросом
}
