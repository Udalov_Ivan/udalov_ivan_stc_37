package ru.innopolis.stc37.udalov.test_db.sql.repositories;

import ru.innopolis.stc37.udalov.test_db.sql.models.Car;

import java.util.List;

public interface CarsRepository  extends  CrudRepository <Car>{

    //SQL console: select * from car limit 3 offset 2;
    //limit - сколько данных всего
    //offset - сколько от самого начала надо сделать отступ

    /**
     * Пагинация
     *
     * @param page - порядковый номер страницы
     * @param size - размер этой странцы
     * @return
     */
    List<Car> findAll(int page, int size);
}
