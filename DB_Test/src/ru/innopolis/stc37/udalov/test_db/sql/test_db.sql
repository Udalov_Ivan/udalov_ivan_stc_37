--запрос на удаление таблицы
DROP TABLE driver;

--создание таблицы
CREATE TABLE driver
(
    id         bigserial primary key, --идентификатор строки (первичный ключ, является уникальным)
    first_name varchar(20),           --20 символов под колонку имя
    last_name  varchar(20),
    age        integer,               --целочисленная колонка
    height     double precision       --рост
);

--указать условия проверки для поля возраста, можно прописать при создании таблицы сразу
ALTER TABLE driver
    ADD CONSTRAINT correct_age CHECK ( age > 0 AND age < 99);
--добавляем дополнительную колонку в таблицу
ALTER TABLE driver
    ADD contract_number char(20) UNIQUE DEFAULT null;
--добавляем колонку стажа
ALTER TABLE driver
    ADD experience integer not null default 0;

--добавляем строки в таблицу
INSERT INTO driver(first_name, last_name, age, height)
VALUES ('DriverOneName', 'DriverOneLastName', 30, 1.80);

INSERT INTO driver(first_name, last_name, age, height)
VALUES ('DriverTwoName', 'DriverTwoLastName', 31, 1.81);

INSERT INTO driver(first_name, last_name, age, height)
VALUES ('DriverThreeName', 'DriverThreeLastName', 40, 1.60);

INSERT INTO driver(first_name, last_name, age, height)
VALUES ('DriverFourName', 'DriverFourLastName', 18, 1.75);

INSERT INTO driver(first_name, last_name, age, height)
VALUES ('DriverFiveName', 'DriverFiveLastName', 27, 1.77);

--удаление строки из таблицы по определенным условиям
DELETE
FROM driver
WHERE id = 3;

INSERT INTO driver(first_name, last_name, age, height)
VALUES ('DriverSixName', 'DriverSixLastName', 24, 1.64);

--обновить контракт у водителя
UPDATE driver
SET contract_number = 'B3'
WHERE id = 6;

--создаем новую таблицу
CREATE TABLE car
(
    id       bigserial primary key,
    color    varchar(20),
    model    varchar(20),
    owner_id bigint,
    FOREIGN KEY (owner_id) REFERENCES driver (id) --связываем обе таблицы
);

--создаем в таблице car строки со значениями привязанными к id из таблицы driver
INSERT INTO car (color, model, owner_id)
VALUES ('Black', 'Lada', 6),
       ('Green', 'BMW', 1),
       ('Yellow', 'Chevrolet', 5),
       ('Black', 'Cadillac', 2),
       ('Red', 'Wolksvagen', 4),
       ('Blue', 'Fiat', null);
--добавляем еще одну машину для единственного владельца (связь "многие к одному")
INSERT INTO car(color, model, owner_id)
VALUES ('Grey', 'Dodge', 1);

--получение всех строк и столбцов из таблицы
SELECT *
FROM driver;

--получение всех столбцов, только тех строк, которые удовлетворяют условиям
SELECT *
FROM driver
WHERE age > 22
  and height > 1.70;

--получение конкретных столбцов
SELECT first_name, last_name
FROM driver;

--получение только уникальных значений из таблицы
SELECT DISTINCT (color)
FROM car;

--создание дополнительной таблицы, для связи "Многие ко многим"
CREATE TABLE drive_car
(
    driver_id bigint,
    car_id    bigint,
    FOREIGN KEY (driver_id) REFERENCES driver (id),
    FOREIGN KEY (car_id) REFERENCES car (id)
);
--переименовать таблицу
ALTER TABLE drive_car
    RENAME TO driver_car;

--добавление нескольких водителей к одной машине driver_car
INSERT INTO driver_car (driver_id, car_id)
VALUES (2, 1),
       (4, 1),
       (5, 1);

INSERT INTO driver_car (driver_id, car_id)
VALUES (1, 5),
       (6, 5),
       (6, 5);

INSERT INTO driver_car (driver_id, car_id)
VALUES (1, 6);

--получить имена владельцев, у которых машина черного цвета
SELECT first_name
FROM driver
WHERE id IN (
    SELECT owner_id
    FROM car
    WHERE color = 'Black');

--получить имена владельцев, у которых во владении более одной машины
SELECT first_name
FROM driver
WHERE id IN (
    SELECT owner_id
    FROM (
             SELECT owner_id,
                    count(*)
                        AS cars_count
             FROM car
             WHERE owner_id notnull
             GROUP BY owner_id) owners_count
    WHERE owners_count.cars_count > 1);

-- JOINS

-- получить всех владельцев и их машины

--Left Join
SELECT *
FROM driver d
         LEFT JOIN car c ON c.owner_id = d.id;

--Right Join
SELECT *
FROM driver d
         FULL JOIN car c ON c.owner_id = d.id;

--Inner Join
SELECT *
FROM driver d
         INNER JOIN car c ON c.owner_id = d.id;

--Full Join
SELECT *
FROM driver d
         FULL JOIN car c ON c.owner_id = d.id;