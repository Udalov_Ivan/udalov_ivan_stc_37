package ru.innopolis.stc37.udalov.test_db.sql.repositories;

import java.sql.ResultSet;
import java.sql.SQLException;

//функциональный интерфейс, который преобразует одну строку в обьет driver
@FunctionalInterface
public interface RowMapper<T> {
    T mapRow(ResultSet row) throws SQLException;
}
