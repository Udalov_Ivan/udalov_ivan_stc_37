package ru.innopolis.stc37.udalov.test_db.sql.repositories;

import ru.innopolis.stc37.udalov.test_db.sql.models.Car;
import ru.innopolis.stc37.udalov.test_db.sql.models.Driver;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.innopolis.stc37.udalov.test_db.sql.utils.JdbcUtil.closeJdbcObjects;

public class DriversRepositoryJdbcImpl implements DriversRepository {

    //statement используется:
    //-запрос выполняется не часто
    //-ненужно подставлять никакие параметры

    //если запрос выполняется часто, скорее всего его необходимо закешировать или необходимо подставить параметры
    // то используем preparedStatement (он умееет кешировать)

    //ctrl + enter вызвать результат в SQL таблице

    //language=SQL
    private static final String SQL_SELECT_ALL_DRIVERS = "SELECT driver.id AS driver_id, * FROM driver";

    //запрос на получение всех владельцев и всех их машин
    // Особенности:
    //1. Повторяются колонки id у машин и у владельца, чтобы избежать неоднозначности - переименовали их:
    // driver.id -> driver_id; car.id -> car_id;
    //2. Записи о владельцах повторяются, потому-что для каждого владельца в каждую строку вписывается его машина
    //3. Есть владельцы без машин, у таких строк car_id и остальная информация по машинам = null
    //4. Если у владельцев есть машины, то у этих машин точно есть owner_id


    //language=SQL
    private static final String SQL_FIND_ALL_DRIVERS_WITH_CARS = "select driver.id as driver_id, c.id as car_id, * " +
            "from driver left join car c on driver.id = c.owner_id order  by driver_id";

    //language=SQL
    private static final String SQL_INSERT_DRIVER = "INSERT  INTO  " +
            "driver(first_name, last_name, age, height) VALUES (?, ?, ?, ?)"; //? - решают проблему конкетинации строк

    //language=SQL
    private static final String SQL_FIND_DRIVER_BY_ID = "SELECT driver.id AS driver_id, * FROM driver WHERE id = ?";

    //language=SQL
    private static final String SQL_UPDATE_DRIVER_BY_ID = "UPDATE driver SET " +
            "first_name = ?, last_name = ?, age = ?, height = ? WHERE  id = ?";

    //функция которая преобразует строку row из базы данных в обьект Driver
    static private final RowMapper<Driver> driverRowMapper = row -> new Driver( //преобразует одну строку в обьект driver
            row.getLong("driver_id"),
            row.getString("first_name"),
            row.getString("last_name"),
            row.getInt("age"),
            row.getDouble("height")
    );

    static final RowMapper<Car> carRowMapper = row -> new Car(
            row.getLong("car_id"),
            row.getString("color"),
            row.getString("model")
    );

    private Map<Long, Driver> drivers; //Long - id, Driver - владелец

    private final RowMapper<Driver> driverWithCarsRowMapper = row -> {
        //преобразуем строку в обект Driver

        Long driverId = row.getLong("driver_id");

        //если еще не встречали такого владельца
        if(!drivers.containsKey(driverId)){
            //то создаем этого водителя
            Driver driver = driverRowMapper.mapRow(row);
            //создаем ему пустой список машин
            driver.setCars(new ArrayList<>());
            // помещаем водителя в мапу под ключом, соответствующий его id
            drivers.put(driverId, driver);
        }
        // смотрим, есть ли машина? Если есть - создаем
        if(row.getObject("car_id") != null){
            Car car = carRowMapper.mapRow(row);
            // нужно в мапе найти водителя, который соответствует этой машине
            Long ownerId = row.getLong("owner_id");
            // получили владельца этой машины из мапы
            Driver driver = drivers.get(ownerId);
            // положим этому водителю машину
            driver.getCars().add(car);
        }
        return null;
    };

    private DataSource dataSource;

    public DriversRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Driver> findAll() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rows = null;

        List<Driver> drivers = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            rows = statement.executeQuery(SQL_SELECT_ALL_DRIVERS); //получили всех водителей

            while (rows.next()) {
                Driver driver = driverRowMapper.mapRow(rows);
                drivers.add(driver);
            }
            //все что открыли, необходимо закрыть
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return drivers;
    }

    @Override
    public Driver findById(long id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Driver driver = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_DRIVER_BY_ID);
            statement.setLong(1, id); //кладем id в данный statement

            rows = statement.executeQuery();

            //нужен только 1 водитель, поэтому вместо while используем if
            if (rows.next()) {
                driver = driverRowMapper.mapRow(rows);
            }
            //все что открыли, необходимо закрыть
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return driver;
    }

    @Override
    public void save(Driver driver) {
        PreparedStatement statement = null; //PreparedStatement выполняет функцию экранирования, не дает выполнить конкатинацию
        ResultSet generatedId = null;
        Connection connection = null;

        try{
            connection = dataSource.getConnection();
            //RETURN_GENERATED_KEYS означает, что запрос должен вернуть ключи(id), которые сгенерировала БД
            // для текущего запроса
            statement = connection.prepareStatement( SQL_INSERT_DRIVER, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, driver.getFirstName());
            statement.setString(2, driver.getLastName());
            statement.setInt(3, driver.getAge());
            statement.setDouble(4, driver.getHeight());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1){ //если affectedRows не равен 1, то не получилось добавить водителя
                throw new SQLException("Can't insert data!");
            }

            //создание id для нового driver
            //ключи которые база сгенерировала сама для этого запроса
            generatedId = statement.getGeneratedKeys();
            //проверяем сгенерировала ли база что либо?
            if (generatedId.next()){
                //получаем ключ, который сгенерировала база для текущего запроса
                driver.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
            } else {
                throw new SQLException("Can't retrieve id!"); //усли id не сгенерирован
            }

        } catch (SQLException e){
            throw  new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public void update(Driver driver) {
        //В отличии от save, update работает над сущностью у которой уже есть id
        PreparedStatement statement = null; //PreparedStatement выполняет функцию экранирования, не дает выполнить конкатинацию
        ResultSet generatedId = null;
        Connection connection = null;

        try{
            connection = dataSource.getConnection();
            // для текущего запроса
            statement = connection.prepareStatement(SQL_UPDATE_DRIVER_BY_ID);

            statement.setString(1, driver.getFirstName());
            statement.setString(2, driver.getLastName());
            statement.setInt(3, driver.getAge());
            statement.setDouble(4, driver.getHeight());
            statement.setLong(5, driver.getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){ //если affectedRows не равен 1, то не получилось добавить водителя
                throw new SQLException("Can't update data!");
            }

        } catch (SQLException e){
            throw  new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public void remove(long id) {

    }

    @Override
    public List<Driver> findAllByAge(int age) {
        return null;
    }

    @Override
    public List<Driver> findAllWithCars() {
        //создаем пустую мапу, где ключ id владельца, значение - обьект Driver
        drivers = new HashMap<>();
        Statement statement = null;
        ResultSet rows = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            //выполняем запрос
            //получаем таблицу со всеми владельцами и машинами
            rows = statement.executeQuery(SQL_FIND_ALL_DRIVERS_WITH_CARS); //получили всех водителей

            //переходим к новой строке, когда вызывается next
            while (rows.next()) {
                // заполняем мапу по правилу, которое описано в driverWithCarsRowMapper
                driverWithCarsRowMapper.mapRow(rows);
            }
            // по итогу в мапе хранятся в качестве значений все владельцы со своими машинами
            // возвращаем список состоящий из значений мапы
            return new ArrayList<>(drivers.values());
            //все что открыли, необходимо закрыть
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
    }


}
