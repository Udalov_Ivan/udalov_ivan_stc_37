package ru.innopolis.stc37.udalov.test_db.sql.repositories;

import ru.innopolis.stc37.udalov.test_db.sql.models.Car;
import ru.innopolis.stc37.udalov.test_db.sql.models.Driver;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ru.innopolis.stc37.udalov.test_db.sql.repositories.DriversRepositoryJdbcImpl.carRowMapper;
import static ru.innopolis.stc37.udalov.test_db.sql.utils.JdbcUtil.closeJdbcObjects;

public class CarsRepositoryJdbcImpl implements CarsRepository {

    //language=SQL
    private final static String SQL_FIND_ALL_CARS_WITH_LIMIT_OFFSET = "SELECT car.id AS car_id, * FROM car LIMIT ? OFFSET ?";

    private DataSource dataSource; //это интерфейс который умеет предоставлять connection по требованию

    public CarsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Car> findAll(int page, int size) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        List<Car> cars = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL_CARS_WITH_LIMIT_OFFSET);
            statement.setInt(1, size); // limit
            statement.setInt(2, size * page); // offset
            rows = statement.executeQuery();

            while (rows.next()) {
                Car car = carRowMapper.mapRow(rows);

                cars.add(car);
            }
            //все что открыли, необходимо закрыть
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return cars;
    }

    @Override
    public List<Car> findAll() {
        return null;
    }

    @Override
    public void save(Car model) {

    }

    @Override
    public void update(Car model) {

    }

    @Override
    public void remove(long id) {

    }

    @Override
    public Car findById(long id) {
        return null;
    }


}
