package ru.innopolis.stc37.udalov.homework9;

public class Main {

    public static void main(String[] args) {
        //реализация λ выражения ru.innopolis.stc37.udalov.homework9.NumbersProcess и stringProcess
        NumbersAndStringProcess numbersAndStringProcess = new NumbersAndStringProcess() {
            @Override
            protected int numbersProcess(int number) {
                return reverseNumber(number);
            }
            @Override
            protected String stringProcess(String process) {
                return reverseString(process);
            }
        };

        NumbersAndStringProcess numbersAndStringProcess1 = new NumbersAndStringProcess() {
            @Override
            protected int numbersProcess(int number) {
                return removeZeros(number);
            }
            @Override
            protected String stringProcess(String process) {
                return removeAllNumbers(process);
            }
        };

        NumbersAndStringProcess numbersAndStringProcess2 = new NumbersAndStringProcess() {
            @Override
            protected int numbersProcess(int number) {
                return evenNumber(number);
            }
            @Override
            protected String stringProcess(String process) {
                return uppercaseLetters(process);
            }
        };

        //разворот числа и строки
        numbersAndStringProcess.processNumber(102);
        numbersAndStringProcess.processString("Sample Text_1");

        //удаление нулей из числа и чисел из строки
        numbersAndStringProcess1.processNumber(30201);
        numbersAndStringProcess1.processString("S7am10ple0 84Te1x4t002_T9w10o777");

        //приводим число к ближайщему четному и делаем строчные буквы прописными
        numbersAndStringProcess2.processNumber(73);
        numbersAndStringProcess2.processString("Sample Text_3 (lowercase letters become uppercase)");

        //вывод информации
        System.out.println("Reversed String and Number: \n" +
                numbersAndStringProcess);
        System.out.println("Removed zeros from Number and removed all numbers from String: \n"
                + numbersAndStringProcess1);
        System.out.println("Nearest even number and uppercase letters for String \n" +
                numbersAndStringProcess2);
    }

    //делаем строчные буквы прописными
    private static String uppercaseLetters(String process) {
        return process.toUpperCase();
    }

    //приводим число к ближайщему четному
    private static int evenNumber(int number) {
        if (number % 2 == 0) {
            return number;
        } else {
            return number - 1;
        }
    }

    //удаление всех чисел из строки
    private static String removeAllNumbers(String process) {
        return process.replaceAll("\\d", "");
    }

    //удаление всех нулей из числа
    private static int removeZeros(int number) {
        if (number != 0) {
            return Integer.parseInt(Integer.toString(number).replace("0", ""));
        }
        return number;
    }

    //разворот числа
    private static int reverseNumber(int number) {
        int numberReversed = 0;

        while (number > 0) {
            numberReversed = numberReversed * 10 + number % 10;
            number = number / 10;
        }
        return numberReversed;
    }

    //разворот строки
    private static String reverseString(String process) {
        if (process.length() <= 1) {
            return process;
        }
        return reverseString(process.substring(1)) + process.charAt(0);
    }

}
