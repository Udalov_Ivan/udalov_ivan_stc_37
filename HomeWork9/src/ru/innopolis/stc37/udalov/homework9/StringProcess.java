package ru.innopolis.stc37.udalov.homework9;

@FunctionalInterface
public interface StringProcess {
    String stringProcess(String process);
}
