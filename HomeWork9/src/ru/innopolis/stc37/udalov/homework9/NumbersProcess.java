package ru.innopolis.stc37.udalov.homework9;

@FunctionalInterface
public interface NumbersProcess {
    int numbersProcess(int number);
}
