package ru.innopolis.stc37.udalov.homework9;

public abstract class NumbersAndStringProcess {
    private static final int MAX_NUMBERS_PROCESSED_COUNT = 6;
    private int processedNumbers[];
    private String processedString[];
    private int processedNumberCount;
    private int processedStringCount;

    public NumbersAndStringProcess() {
        this.processedNumbers = new int[MAX_NUMBERS_PROCESSED_COUNT];
        this.processedString = new String[MAX_NUMBERS_PROCESSED_COUNT];
    }

    //заполнение массива числами
    public void processNumber(int number) {
        if (processedNumberCount < MAX_NUMBERS_PROCESSED_COUNT) {
            int processedNumber = numbersProcess(number);
            saveInputNumber(processedNumber);
        } else {
            System.err.println("Array is totally full! Please reduce the amount of data entered.");
        }
    }

    //заполнение массива строками
    public void processString(String process) {
        if (processedStringCount < MAX_NUMBERS_PROCESSED_COUNT) {
            String processedString = stringProcess(process);
            saveInputString(processedString);
        } else {
            System.err.println("Array is totally full! Please reduce the amount of data entered.");
        }
    }

    private void saveInputNumber(int number) {
        processedNumbers[processedNumberCount++] = number;
    }

    private void saveInputString(String process) {
        processedString[processedStringCount++] = process;
    }


    public int showProcessedNumber() {
        int a = 0;
        for (int i = 0; i < processedNumberCount; i++) {
            a = processedNumbers[i];
        }
        return a;
    }

    public String showProcessedString() {
        String b = " ";
        for (int i = 0; i < processedStringCount; i++) {
            b = processedString[i];
        }
        return b;
    }

    //вывод на печать результата для чисел и строк
    public String toString() {
        return "Result for numbers: " +
                showProcessedNumber() +
                ";\n" +
                "Result for strings: " +
                showProcessedString() +
                ";\n"
                ;
    }

    //абстрактные методы
    protected abstract int numbersProcess(int number);

    protected abstract String stringProcess(String process);


}
