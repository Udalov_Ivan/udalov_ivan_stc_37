package ru.innopolis.stc37.udalov.homework3;

import java.util.Scanner;
class Task3_2{
    
    public static double f (double x){ 
        return x * x;
    }
   
    public static double calcSimpsonIntegral(double a, double b, int n){ 
        
        double h = (b - a) / (2 * n);

        double simpsonIntegral = 0;
        
        for (double i = a; a <= b; a+= h){ 
        
            double partOfTheInterval = (f(a) + 4 * f(a / 2) + f(a)) * (h / 3);
            simpsonIntegral += partOfTheInterval; 
        }
        return simpsonIntegral; 
    }

    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Value For a: ");
        double a = scanner.nextDouble();
        System.out.println("Enter Value For b: ");
        double b = scanner.nextDouble();
        System.out.println("Enter Value For n: ");
        int n = scanner.nextInt(); ;
        
        
        System.out.println("Simpson Integral is: " + calcSimpsonIntegral(a, b, n) + ";");
    }
}