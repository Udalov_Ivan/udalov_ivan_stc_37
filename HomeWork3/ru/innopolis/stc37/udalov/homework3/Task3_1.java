package ru.innopolis.stc37.udalov.homework3;

import java.util.Arrays;
import java.util.Scanner;
class Task3_1{    
    
    public static void main(String []args){
        System.out.println("Enter array size: ");
            
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];

        System.out.println("Enter array numbers: ");
        
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println("Initial Array: \n" + Arrays.toString(array) + "\n");
        
        sumArray(array); // 1. вызов суммы массива
        
        reverseArrayPosition(array); // 2. отзеркаливание массива
        
        arithmeticMean(array); // 3. средняя арифметическая самма элементов массива
        
        swapMinToMax(array); // 4. смена местами максимального и минимального элементов массива
        
        isArraySorted(array); // 5. метод "Пузырька"
        
        arrayToNumber(array); // 6. перевод массива в число
        
    }
	
        
    public static void sumArray(int array[]){ // 1. вызов суммы массива
        
        int sumArray = 0;
        for (int i = 0; i < array.length; i++){
            sumArray +=array[i];
        }
		System.out.println("Array Sum: " + sumArray + ";\n");
    }
    
    
	public static void reverseArrayPosition(int array[]){ // 2. отзеркаливание массива
		
		for (int i = 0; i < array.length / 2; i++){
			int changePosition = array[array.length-i-1];
			array[array.length-i-1] = array[i];
			array[i] = changePosition;
		}
		System.out.println("Reversed Array: \n" + Arrays.toString(array) + "\n");
	}


    public static void arithmeticMean(int array[]){ // 3. средняя арифметическая самма элементов массива
        
        double sum = 0;
		double arithmeticMean = 0;
    
        for (int i = 0; i < array.length; i++){
            sum += array[i];
            arithmeticMean = sum / array.length;
        }
        System.out.println("Arithmetic Mean Of Array: " + arithmeticMean + ";\n");
    }


    public static void swapMinToMax(int array[]){ // 4. смена местами максимального и минимального элементов массива
        
        int min = array[0];
        int positionOfMin = 0;
        int max = array[0];
        int positionOfMax = 0;
        
        for (int i = 1; i < array.length; i++){
                
            if (array[i] < min){
                min = array[i];
                positionOfMin = i;
            }
            
            if (array[i] > max){
                max = array[i];
                positionOfMax = i;
            }
        }
        
        int temp = array[positionOfMax];
        array[positionOfMax] = array[positionOfMin];
        array[positionOfMin] = temp;
        
        System.out.println("Swap Array From Min to Max: \n" + Arrays.toString(array) + "\n");
    }


    public static void isArraySorted(int array[]){ // 5. метод "Пузырька"
        boolean isArraySorted = false;
        int temp;
        
        while (!isArraySorted){
            isArraySorted = true;
            
            for (int j = 0; j < array.length - 1; j++){
                if (array[j] > array[j + 1]){
                    isArraySorted = false;
                    
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println("\"Bubble Sorted\" Array: \n" + Arrays.toString(array) + "\n");
    }  


    public static void arrayToNumber(int array[]){ // 6. перевод массива в число по модулю, при вводе числа > 1000, выводит сообщение об ошибке
        int number = 0;
        
        for (int i = 0; i < array.length; i++){
            if (array[i] < 10){
            number = number * 10 + Math.abs(array[i]);
            }
            
            if ((array[i] >= 10)&&(array[i] < 100)){
            number = number * 100 + Math.abs(array[i]);
            }
            
            if ((array[i] >= 100)&&(array[i] < 1000)){
            number = number * 1000 + Math.abs(array[i]);
            }
            else if (array[i] >= 1000){
            System.out.println("Attention! Entered number is greater than 999, number is Not correct;");
            }
        }        
        System.out.println("Array to number: " + number + ";"); 
    }

}