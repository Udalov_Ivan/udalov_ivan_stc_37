package ru.innopolis.stc37.udalov.homework12;

/**
 * 18.03.2021
 * 28. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// String lines[] = new String[10]; - обычный массив, ключ(индекс) - целое число
// lines[2] = "Привет"; System.out.println(lines[2]);
// значение - строка
// inno.primitive.Map<String, String> map = new ...;
// map.put("Марсель", "Сидиков"); - ключ - строка, значение - строка
// System.out.println(map.get("Марсель"); // Сидиков
public interface Map<K, V> {
    // положить значение value под ключом key
    // a[5] = 7 <-> map.put(5, 7);
    void put(K key, V value);
    // получить значение по ключу
    // int i = a[5] <-> int i = map.get(5);
    V get(K key);
}
