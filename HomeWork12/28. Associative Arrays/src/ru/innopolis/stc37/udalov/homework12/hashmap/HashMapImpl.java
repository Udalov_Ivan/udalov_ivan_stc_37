package ru.innopolis.stc37.udalov.homework12.hashmap;

import ru.innopolis.stc37.udalov.homework12.Map;

/**
 * 18.03.2021
 * 28. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[];

    public HashMapImpl() {
        this.entries = new MapEntry[DEFAULT_SIZE];
    }

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        // получаем хеш-код у ключа - это и будет индекс массива, где лежит значение
        // 4232 -> 1
        //  1010010101010011
        // &            1111
        //  0000000000000011

        // посчитали индекс, в который мы хотим положить элемент
        int index = key.hashCode() & (entries.length - 1);

        // проверяем, а не лежит ли там уже какой-то элемент?

        if (entries[index] != null) {
            // проверить, нет ли там аналогичного ключа
            MapEntry<K, V> current = entries[index];
            while (current != null) {
                // если мы нашли ключ, который совпал с тем, который мы подали на вход - делаем замену значения
                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                // если не совпал - идем дальше
                current = current.next;
            }
            // если мы оказали здесь - значит ни разу такого ключа не встретили
            // положить текущую пару ключ-значение в таблицу

            // создали новую пару
            MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
            // текущая пара стала первой - следующий для нее элемент - это тот, который был в начале списке
            newMapEntry.next = entries[index];
            // делаем его первым в таблице
            entries[index] = newMapEntry;
        } else {
            entries[index] = new MapEntry<>(key, value);
        }
    }

    //реализация метода get
    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        MapEntry<K, V> current = entries[index];

        if (entries[index] != null) {
            while (current != null) {
                if (current.key.equals(key)) {
                    return current.value;
                }
                current = current.next;
            }

            MapEntry<K, V> next = entries[index];
            entries[index] = next;
            return next.value;
        }
        return current.value;

    }

}
