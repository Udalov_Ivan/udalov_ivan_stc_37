package ru.innopolis.stc37.udalov.homework12;

import ru.innopolis.stc37.udalov.homework12.hashmap.HashMapImpl;

public class Main {

    public static void main(String[] args) {
	    Map<String, String> map = new HashMapImpl<>();

	    map.put("Марсель", "Сидиков");
	    map.put("Виктор", "Евлампьев");
	    map.put("Айрат", "Мухутдинов");
	    map.put("Даниил", "Вдовинов");
	    map.put("Даниил", "Богомолов");
	    map.put("Джамиль", "Садыков");
	    map.put("Николай", "Пономарев");
	    map.put("Siblings", "HELLO1");
	    map.put("Teheran", "HELLO2");
	    map.put("Марсель", "Гудайдиев");

		map.put("TEST_1", "TestMessage_1");
		map.put("TEST_1", "TestMessage_2");
		map.put("TEST_2", "TestMessage_3");

	    //Вывод на печать по методу get
		System.out.println(" key \"Даниил\" -> value: " + map.get("Даниил")); // перезапись значения - Вдовинов -> Богомолов
		System.out.println(" key \"Айрат\" -> value: " + map.get("Айрат")); // Мухутдинов
		System.out.println(" key \"Виктор\" -> value: " + map.get("Виктор")); // Евлампьев
		System.out.println(" key \"Джамиль\" -> value: " + map.get("Джамиль")); // Садыков
		System.out.println(" key \"Николай\" -> value: " + map.get("Николай")); // Пономарев
		System.out.println(" key \"Siblings\" -> value: " + map.get("Siblings")); // HELLO1 ("Siblings", "Teheran", "Марсель" имеют одинковый HashCode)
		System.out.println(" key \"Teheran\" -> value: " + map.get("Teheran")); // HELLO2 ("Siblings", "Teheran", "Марсель" имеют одинковый HashCode)
		System.out.println(" key \"Марсель\" -> value: " + map.get("Марсель")); // перезапись значения - Сидиков -> Гудайдиев ("Siblings", "Teheran", "Марсель" имеют одинковый HashCode)
		System.out.println(" key \"TEST_1\" -> value: " + map.get("TEST_1")); // перезапись  значения - TestMessage_1 -> TestMessage_2
		System.out.println(" key \"TEST_2\" -> value: " + map.get("TEST_2")); // TestMessage_3

    }
}
