package ru.innopolis.stc37.udalov.homework10;

import org.junit.jupiter.api.Test;
import ru.innopolis.stc37.udalov.homework10.InnoLinkedList;


import static org.junit.jupiter.api.Assertions.*;

class InnoLinkedListTest {
    InnoLinkedList innoLinkedList = new InnoLinkedList();

    @Test
    void insert() {
        addElements();

        //заменяем элемент по индексу
        innoLinkedList.insert(0, 777);

        assertEquals(5, innoLinkedList.size(), "Замена элемента по индексу - размер списка не должен измениться");
    }

    @Test
    void addToBegin() {
        addElements();

        //добавляем элемент в начало списка по значению
        innoLinkedList.addToBegin(11);

        assertEquals(6, innoLinkedList.size(), "Добавление элемента в начало списка по значению - размер списка должен увеличиться");
    }

    @Test
    void removeByIndex() {
        addElements();

        //удаление элемента по индексу
        innoLinkedList.removeByIndex(1);

        assertEquals(4, innoLinkedList.size(), "Удалили элемент из списка по индексу - список должен уменьшиться");
    }

    @Test
    void remove() {
        addElements();

        //удаление элемента по значению
        innoLinkedList.remove(4);

        assertEquals(4, innoLinkedList.size(), "Удалили элемент из списка по значению - список должен уменьшиться");
    }

    @Test
    void contains() {
        addElements();

        boolean testNumber = innoLinkedList.contains(4);

        //ожидаемый результат - true;
        assertTrue(testNumber);
    }

    private void addElements() {
        innoLinkedList.add(1);
        innoLinkedList.add(2);
        innoLinkedList.add(3);
        innoLinkedList.add(4);
        innoLinkedList.add(5);
    }
}