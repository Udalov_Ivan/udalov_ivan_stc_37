package ru.innopolis.stc37.udalov.homework10;

import org.junit.jupiter.api.Test;
import ru.innopolis.stc37.udalov.homework10.InnoArrayList;

import static org.junit.jupiter.api.Assertions.*;

class InnoArrayListTest {
    InnoArrayList innoArrayList = new InnoArrayList();

    @Test
    void insert() {
        addElements();

        //заменяем элемент по индексу
        innoArrayList.insert(3, 44);

        assertEquals(6, innoArrayList.size(), "Замена элемента по индексу - размер массива не должен измениться");
    }

    @Test
    void addToBegin() {
        addElements();

        //добавляем элемент в начало списка по значению
        innoArrayList.addToBegin(11);

        assertEquals(7, innoArrayList.size(), "Добавление элемента в начало массива - размер массива должен увеличиться");
    }

    @Test
    void removeByIndex() {
        addElements();

        //удаление элемента по индексу
        innoArrayList.removeByIndex(1);

        assertEquals(5, innoArrayList.size(), "Удалили элемент из массива по индексу - размер массива должен уменьшиться");
    }

    @Test
    void remove() {
        addElements();

        //удаление элемента из массива по значению
        innoArrayList.remove(11);

        assertEquals(5, innoArrayList.size(), "Удалили элемент из массива по значению - размер массива должен уменьшиться");
    }

    @Test
    void contains() {
        addElements();

        //тестовый массив с набором чисел для проверки
        int[] testArray = {16, 15, 14, 13, 12, 11};

        //есть ли числа из тестового массива в исходном
        for (int j : testArray) {
            boolean testArrayList = innoArrayList.contains(j);
            assertTrue(testArrayList);
        }
    }

    private void addElements() {
        innoArrayList.add(11);
        innoArrayList.add(12);
        innoArrayList.add(13);
        innoArrayList.add(14);
        innoArrayList.add(15);
        innoArrayList.add(16);
    }
}