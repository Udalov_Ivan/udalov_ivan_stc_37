package ru.innopolis.stc37.udalov.homework10;

public class Main {

    public static void main(String[] args) {
        System.out.println("LinkedList:");
        // создал список list
        InnoList list = new InnoLinkedList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);

        //значение исходного связанного списка
        printList((InnoList) list);

        //получить значение элемента по индексу
        System.out.println(list.get(0));

        //замена элемента по индексу
        list.insert(1,111);
        printList(list);

        //добавить элемент в начало списка
        list.addToBegin(888);
        printList(list);

        //удаление по индексу
        list.removeByIndex(7);
        printList(list);

        //удаление элемента
        list.remove(20);
        printList(list);

        //поиск элемента в коллекции
        System.out.println(list.contains(12)); // true/false

        System.out.println("----------------------------");

        System.out.println("ArrayList:");
        // создал список list2
        InnoList list2 = new InnoArrayList();

        list2.add(1);
        list2.add(41);
        list2.add(12);
        list2.add(32);
        list2.add(99);
        list2.add(100);

        printList(list2);

        //получить значение элемента по индексу
        System.out.println(list2.get(0));

        //замена элемента по индексу
        list2.insert(5, 333);
        printList(list2);

        //добавление элемента в начало
        list2.addToBegin(777);
        printList(list2);

        //удаление по индексу
        list2.removeByIndex(1);
        printList(list2);

        //удаление элемента
        list2.remove(12);
        printList(list2);

        //поиск элемента в коллекции
        System.out.print(list2.contains(1));

    }

    //вывод значений списка
    private static void printList(InnoList list) {
        InnoIterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }

}
