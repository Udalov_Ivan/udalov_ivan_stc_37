package ru.innopolis.stc37.udalov.homework10;

/**
 * 11.03.2021
 * 24. Collections
 *
 * Итератор - курсор, который позволяет последовательно обходить какую-либо коллекцию
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface InnoIterator {
    /**
     * Возвращает следующий элемент коллекции
     * @return элемент
     */
    int next();

    /**
     * Проверяет, есть ли следующий элемент коллекции
     * @return true - если есть, false - если нет
     */
    boolean hasNext();
}
