package ru.innopolis.stc37.udalov.homework10;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int[] elements;

    private int count; //сколько сейчас элементов

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index]; //получение значения по индексу
        } else {
            return -1; // -1 если вышли за границу
        }
    }

    @Override
    public void insert(int index, int element) {
        int[] temp = elements;
        for (int i = 0; i < elements.length; i++) {
            temp[index] = element;
        }
    }

    @Override
    public void addToBegin(int element) {
        int[] tempArray = {element};
        int[] newElements = new int[elements.length + tempArray.length];

        for (int i = 0; i < elements.length; i++) {

            newElements[i + tempArray.length] = elements[i];
            elements[i] = newElements[i];
        }
        for (int j = 0; j < tempArray.length; j++) {

            elements[j] = tempArray[j];
        }
        count++;
    }

    @Override
    public void removeByIndex(int index) {

        int[] newElements = new int[elements.length - 1];

        for (int i = 0; i < elements.length; i++) {
            if (i < index) {
                newElements[i] = elements[i];
            } else if (i > index) {
                newElements[i - 1] = elements[i];
            }
        }
        this.elements = newElements;
        elements[count--] = index;

    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }
        elements[count++] = element; //добавить значение в массив
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int[] newElements = new int[elements.length + elements.length / 2]; //увеличение изначального массива в 1.5 раза
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        //поиск индекса массива
        int position = -1;

        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                position = i;
                break;
            }
        }
        removeByIndex(position);
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < elements.length; i++) {

            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}
