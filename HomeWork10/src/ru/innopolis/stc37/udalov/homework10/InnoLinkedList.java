package ru.innopolis.stc37.udalov.homework10;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
// Минусы - медленный доступ по  индексу
public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;

    private int count;

    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        Node current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        current.value = element;
    }

    @Override
    public void addToBegin(int element) {
        Node newNode = new Node(element);
        Node oldFirst = first;
        first = newNode;
        first.next = oldFirst;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        Node current = first;

        if (index == 0) {
            first = current.next;
        } else {
            for (int i = 0; i < index - 1; i++) {
                current = current.next;
            }
            current.next = current.next.next;
        }
        count--;
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {

        while (first != null && first.value == element) {
            first = first.next;
        }

        Node current = first;

        while (current.next != null) {
            if (current.next.value == element) {
                current.next = current.next.next;
            } else {
                current = current.next;
            }
        }
        count--;
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        if (current.value == element) {
            return true;
        }
        while (current.next != null) {
            current = current.next;

            if (current.value == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current != null;
        }

    }
}
