package ru.innopolis.stc37.udalov.project.services;

import ru.innopolis.stc37.udalov.project.dto.StatisticDto;
import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.models.Shot;
import ru.innopolis.stc37.udalov.project.repository.GamesRepository;
import ru.innopolis.stc37.udalov.project.repository.PlayersRepository;
import ru.innopolis.stc37.udalov.project.repository.ShotsRepository;

import java.time.LocalDateTime;

//бизнес-логика
public class GameServiceImpl implements GameService {

    private final PlayersRepository playersRepository;
    private final GamesRepository gamesRepository;
    private final ShotsRepository shotsRepository;
    //изменение цвета текста выводимого в консоль
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    //конструктор
    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String ipOne, String ipTwo, String playerOneName, String playerTwoName) {
        System.out.println("[GET " +
                "IP_Player_One:" + ipOne +
                " IP_Player_Two:" + ipTwo +
                " Player_One_Name:" + playerOneName +
                " Player_Two_Name:" + playerTwoName +
                "]");
        //выполняем метод checkIfPlayersExists для получения информации об игроках и их сохранение
        Player playerOne = checkIfPlayersExists(ipOne, playerOneName);
        Player playerTwo = checkIfPlayersExists(ipTwo, playerTwoName);

        //создание новой игры
        Game game = Game.builder()
                .dateOfGame(LocalDateTime.now())
                .playerOne(playerOne)
                .playerTwo(playerTwo)
                .playerOneShootsCounters(0)
                .playerTwoShootsCounters(0)
                .timeOfGame(0L)
                .build();

        //сохранение в репозитории игры
        gamesRepository.save(game);
        // возвращаем идентификактор игры
        return game.getId();
    }

    private Player checkIfPlayersExists(String ip, String playerName) {
        // получение информации об обоих игроках
        Player player = playersRepository.findByPlayerName(playerName);
        // если нет таких игроков
        if (player == null) {
            //тогда создать игрока
            player = Player.builder()
                    .playerName(playerName)
                    .playerIpAddress(ip)
                    .maxPoint(0)
                    .winCount(0)
                    .loseCount(0)
                    .build();

            //сохраняем игрока
            playersRepository.save(player);
        } else {
            //если такой игрок в системе уже был, обновляем ip-адресс и информацию о нем
            player.setPlayerIpAddress(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String playerWhoShoot, String playerWhoTarget) {

        //сообщение о выстреле от клиента на сервер
        System.out.println("[Game_ID:" + gameId + " " +
                "<" + playerWhoShoot + ">" +
                " hit " +
                "<" + playerWhoTarget + ">]");

        //получаем того, кто стрелял из репозитория
        Player shooterPlayer = playersRepository.findByPlayerName(playerWhoShoot);
        //получаем того, в кого стреляли из репозитория
        Player targetPlayer = playersRepository.findByPlayerName(playerWhoTarget);
        //получаем игру
        Game game = gamesRepository.findById(gameId);
        //создаем выстрел
        Shot shot = Shot.builder()
                .timeToShoot(LocalDateTime.now())
                .game(game)
                .playerWhoShoot(shooterPlayer)
                .playerWhoIsTheTarget(targetPlayer)
                .build();

        //учитывание попадагий и увеличении очков
        shooterPlayer.setMaxPoint(shooterPlayer.getMaxPoint() + 1);

        //кто из игроков сделал выстрел
        //если стрелял первый игрок
        if (game.getPlayerOne().getPlayerName().equals(playerWhoShoot)) {
            //сохроняем информацию о выстреле в игре
            game.setPlayerOneShootsCounters(game.getPlayerOneShootsCounters() + 1);
        }
        //если стрелял второй игрок
        if (game.getPlayerTwo().getPlayerName().equals(playerWhoShoot)) {
            //сохроняем информацию о выстреле в игре
            game.setPlayerTwoShootsCounters(game.getPlayerTwoShootsCounters() + 1);
        }

        //дата и время проведения игры
        game.setDateOfGame(LocalDateTime.now());

        //обнавляем данные среляющего игрока в репозитории
        playersRepository.update(shooterPlayer);
        //обновление данных игры в репозитории
        gamesRepository.update(game);
        //сохранение выстрела в репозиторий
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId, long second) {
        StatisticDto statisticDto = new StatisticDto();
        final Game game = gamesRepository.findById(gameId);

        //имя игрока
        final String playerNameOne = game.getPlayerOne().getPlayerName();
        final String playerNameTwo = game.getPlayerTwo().getPlayerName();
        //попадания
        final int hitPlayerOne = game.getPlayerOneShootsCounters();
        final int hitPlayerTwo = game.getPlayerTwoShootsCounters();
        //максимальное количество очков
        final int maxPointPlayerOne = game.getPlayerOne().getMaxPoint();
        final int maxPointPlayerTwo = game.getPlayerTwo().getMaxPoint();
        //количество побед
        int maxWinPlayerOne = game.getPlayerOne().getWinCount();
        int maxWinPlayerTwo = game.getPlayerTwo().getWinCount();
        //количество поражений
        int loseCountPlayerOne = game.getPlayerOne().getLoseCount();
        int loseCountPlayerTwo = game.getPlayerTwo().getLoseCount();

        final Player playerFirst = playersRepository.findByPlayerName(playerNameOne);
        final Player playerSecond = playersRepository.findByPlayerName(playerNameTwo);

        game.setTimeOfGame(second);

        //вывод информации при завершение игры
        statisticDto.setMessage(finishGameLogic(statisticDto, game, playerFirst,
                playerSecond, playerNameOne, playerNameTwo,
                hitPlayerOne, hitPlayerTwo, maxPointPlayerOne, maxPointPlayerTwo,
                maxWinPlayerOne, maxWinPlayerTwo, loseCountPlayerOne, loseCountPlayerTwo));

        //сохраняем изменения
        playersRepository.update(playerFirst);
        playersRepository.update(playerSecond);
        gamesRepository.update(game);

        //время затраченное на игру
        System.out.println(ANSI_BLUE + "Time Of Game: " + game.getTimeOfGame() + " seconds;" + ANSI_RESET);

        return statisticDto;
    }

    //условия окончания игры и вывод результатов
    private String finishGameLogic(StatisticDto statisticDto, Game game, Player playerFirst,
                                 Player playerSecond, String playerNameOne, String playerNameTwo,
                                 int hitPlayerOne, int hitPlayerTwo, int maxPointPlayerOne, int maxPointPlayerTwo,
                                 int maxWinPlayerOne, int maxWinPlayerTwo, int loseCountPlayerOne, int loseCountPlayerTwo) {
        //идентификатор игры
        System.out.println(ANSI_BLUE +
                "\nGame id is: " + game.getId() +
                "; \nDate of Game: " + game.getDateOfGame() +
                ";" + ANSI_RESET);

        //еслы выиграл первый игрок
        if (hitPlayerOne > hitPlayerTwo) {
            playerFirst.setWinCount(playerFirst.getWinCount() + 1);
            playerSecond.setLoseCount(playerSecond.getLoseCount() + 1);

            playersInfo(playerNameOne, playerNameTwo, hitPlayerOne, hitPlayerTwo, maxPointPlayerOne,
                    maxPointPlayerTwo, playerFirst.getWinCount(), maxWinPlayerTwo, loseCountPlayerOne, playerSecond.getLoseCount());
            System.out.println(ANSI_BLUE +
                    "Winner is: " + playerNameOne +
                    ";" + ANSI_RESET);

            //сообщение для отправки клиенту
            return "[Winner_is:" + playerNameOne + "]" +
                    "[Player_one_is:" + playerNameOne + ";" +
                    "Hit=" + hitPlayerOne + ";" +
                    "Max_Points=" + maxPointPlayerOne + ";" +
                    "Win_Counts=" + playerFirst.getWinCount() + ";" +
                    "Lose_Count=" + loseCountPlayerOne + ";]" +
                    "[Player_two_is:" + playerNameTwo + ";" +
                    "Hit=" + hitPlayerTwo + ";" +
                    "Max_Points=" + maxPointPlayerTwo + ";" +
                    "Win_Counts=" + maxWinPlayerTwo + ";" +
                    "Lose_Count=" + playerSecond.getLoseCount() + ";]" +
                    "[Time_of_Game:" + game.getTimeOfGame() + "]" +
                    "[Game_ID_is:" + game.getId() + "]";
        }

        //если выиграл второй игрок
        else if (hitPlayerOne < hitPlayerTwo) {
            playerSecond.setWinCount(playerSecond.getWinCount() + 1);
            playerFirst.setLoseCount(playerFirst.getLoseCount() + 1);

            playersInfo(playerNameOne, playerNameTwo, hitPlayerOne, hitPlayerTwo, maxPointPlayerOne,
                    maxPointPlayerTwo, maxWinPlayerOne, playerSecond.getWinCount(), playerFirst.getLoseCount(), loseCountPlayerTwo);
            System.out.println(ANSI_BLUE +
                    "Winner is: " + playerNameTwo +
                    ";" + ANSI_RESET);

            //сообщение для отправки клиенту
            return "[Winner_is:" + playerNameTwo + "]" +
                    "[Player_one_is:" + playerNameOne + ";" +
                    "Hit=" + hitPlayerOne + ";" +
                    "Max_Points=" + maxPointPlayerOne + ";" +
                    "Win_Counts=" + maxWinPlayerOne + ";" +
                    "Lose_Count=" + playerFirst.getLoseCount() + ";]" +
                    "[Player_two_is:" + playerNameTwo + ";" +
                    "Hit=" + hitPlayerTwo + ";" +
                    "Max_Points=" + maxPointPlayerTwo + ";" +
                    "Win_Counts=" + playerSecond.getWinCount() + ";" +
                    "Lose_Count=" + loseCountPlayerTwo + ";]" +
                    "[Time_of_Game:" + game.getTimeOfGame() + "]" +
                    "[Game_ID_is:" + game.getId() + "]";

            //если ничья
        } else {
            playersInfo(playerNameOne, playerNameTwo, hitPlayerOne, hitPlayerTwo, maxPointPlayerOne,
                    maxPointPlayerTwo, maxWinPlayerOne, maxWinPlayerTwo, loseCountPlayerOne, loseCountPlayerTwo);
            System.out.println(ANSI_BLUE + "Nobody won because it’s a Draw!;" + ANSI_RESET);
            return "Draw";
        }

    }

    //результаты игры на сервере
    private void playersInfo(String playerNameOne, String playerNameTwo, int hitPlayerOne, int hitPlayerTwo, int maxPointPlayerOne,
                             int maxPointPlayerTwo, int maxWinPlayerOne, int maxWinPlayerTwo, int loseCountPlayerOne, int loseCountPlayerTwo) {
        System.out.println(ANSI_BLUE + "Player One: " + playerNameOne +
                "; hit = " + hitPlayerOne +
                "; Max Point = " + maxPointPlayerOne +
                "; Win Count = " + maxWinPlayerOne +
                "; Lose Count = " + loseCountPlayerOne +
                ";");
        System.out.println("Player Two: " + playerNameTwo +
                "; hit = " + hitPlayerTwo +
                "; Max Point = " + maxPointPlayerTwo +
                "; Win Count = " + maxWinPlayerTwo +
                "; Lose Count = " + loseCountPlayerTwo +
                ";" + ANSI_RESET);
    }

}
