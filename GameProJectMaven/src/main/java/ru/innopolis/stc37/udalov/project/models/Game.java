package ru.innopolis.stc37.udalov.project.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Game {
    private Long id;
    private LocalDateTime dateOfGame;
    private Player playerOne;
    private Player playerTwo;
    private Integer playerOneShootsCounters;
    private Integer playerTwoShootsCounters;
    private Long timeOfGame;
}
