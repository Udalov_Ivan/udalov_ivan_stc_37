package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Shot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ShotsRepositoryFileImpl implements ShotsRepository {

    private String dbFileName;
    private String sequenceFileName;

    public ShotsRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    //при save происходит запись в файл
    @Override
    public void save(Shot shot) {

        try {
            //записываем информацию в файл
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true));
            //генератор идентификатора
            shot.setId(generateId());
            //перечень параметров записываемых в файле
            writer.write(shot.getId() +
                    "#" + shot.getTimeToShoot().toString() +
                    "#" + shot.getGame().getId() +
                    "#" + shot.getPlayerWhoShoot().getPlayerName() +
                    "#" + shot.getPlayerWhoIsTheTarget().getPlayerName() + "\n");
            //закрываем поток
            writer.close();
        } catch (IOException e) {
            //выбрасываем исключение, если не удалось открыть файл
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Shot findById(Long shotId) {
        return null;
    }

    @Override
    public void update(Shot shot) {

    }

    private Long generateId() {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            //читаем последний сгенерированный id как строку
            String lastGeneratedIdAsString = reader.readLine();
            //преобразуем ее в Long
            long id = Long.parseLong(lastGeneratedIdAsString);
            //закрываем поток
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            //записываем в файл значение на 1 больше
            writer.write(String.valueOf(id + 1));
            writer.close();

            //возвращаем идентификатор
            return id;

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
