package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Player;

import java.util.HashMap;
import java.util.Map;

public class PlayersRepositoryMapImpl implements PlayersRepository {

    private Map<String, Player> players;

    //конструктор для Map
    public PlayersRepositoryMapImpl() {
        players = new HashMap<>();
    }

    //найти игрока по имени
    @Override
    public Player findByPlayerName(String playerName) {
        return players.get(playerName);
    }

    //сохранить информацию о игроке
    @Override
    public void save(Player player) {
        players.put(player.getPlayerName(), player);

    }

    //обновитть информацию о игроке
    @Override
    public void update(Player player) {
        if (players.containsKey(player.getPlayerName())) {
            players.put(player.getPlayerName(), player);
        } else {
            System.err.println("Can't update a non-existent player");
        }
    }
}
