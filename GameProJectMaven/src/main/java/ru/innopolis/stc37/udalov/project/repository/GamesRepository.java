package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;

public interface GamesRepository {
    //сохранение игры в базе
    void save(Game game);

    //найти игру по идентификатору
    Game findById(Long gameId);

    //обновление информации о игре
    void update(Game game);
}
