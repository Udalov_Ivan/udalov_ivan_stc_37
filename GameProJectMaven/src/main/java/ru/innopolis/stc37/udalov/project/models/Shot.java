package ru.innopolis.stc37.udalov.project.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Shot {
    private Long id;
    private LocalDateTime timeToShoot;
    private Game game;
    private Player playerWhoShoot;
    private Player playerWhoIsTheTarget;
}
