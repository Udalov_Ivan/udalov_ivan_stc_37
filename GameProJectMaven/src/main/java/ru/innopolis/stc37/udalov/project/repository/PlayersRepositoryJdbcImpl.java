package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.utils.JdbcUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_INSERT_PLAYER = "INSERT INTO " +
            "player (player_ip_address, player_name, max_point, win_count, lose_count) VALUES (?, ?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NAME = "SELECT player_name, * FROM player WHERE player_name = ?";

    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_ID = "UPDATE player SET " +
            "player_ip_address = ?, player_name = ?, max_point = ?, win_count = ?, lose_count = ? WHERE id = ?";

    //функция которая преобразует строку row из базы данных в обьект Player
    static private final RowMapper<Player> playerRowMapper = JdbcUtil::mapPlayerRow;

    private final DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByPlayerName(String playerName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NAME)) {
            //кладем playerName в данный statement
            statement.setString(1, playerName);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             //RETURN_GENERATED_KEYS - запрос должен вернуть ключи(id), которые сгенерировала БД
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getPlayerIpAddress());
            statement.setString(2, player.getPlayerName());
            statement.setInt(3, player.getMaxPoint());
            statement.setInt(4, player.getWinCount());
            statement.setInt(5, player.getLoseCount());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't insert player!");
            }
            //создание id для нового игрока
            //ключи которые база сгенерировала сама для этого запроса
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                //проверяем сгенерировала ли база что либо?
                if (generatedId.next()) {
                    //получаем ключ, который сгенерировала база для текущего запроса
                    player.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
                } else {
                    throw new SQLException("Can't retrieve id!"); //если id не сгенерирован
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_ID)) {
            statement.setString(1, player.getPlayerIpAddress());
            statement.setString(2, player.getPlayerName());
            statement.setInt(3, player.getMaxPoint());
            statement.setInt(4, player.getWinCount());
            statement.setInt(5, player.getLoseCount());
            statement.setLong(6, player.getId());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't update player!");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
