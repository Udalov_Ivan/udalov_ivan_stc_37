package ru.innopolis.stc37.udalov.project.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.innopolis.stc37.udalov.project.dto.StatisticDto;
import ru.innopolis.stc37.udalov.project.repository.*;
import ru.innopolis.stc37.udalov.project.services.GameService;
import ru.innopolis.stc37.udalov.project.services.GameServiceImpl;
import ru.innopolis.stc37.udalov.project.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {

    //изменение цвета текста выводимого в консоль
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RESET = "\u001B[0m";
    //для записи в файлы
    public static final String SHOTS_DB_TXT = "shots_db.txt";
    public static final String SHOTS_SEQUENCE_TXT = "shots_sequence.txt";
    public static final String PLAYER_DB_TXT = "player_db.txt";
    public static final String PLAYER_SEQUENCE_TXT = "player_sequence.txt";
    //JDBC
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/game_project_db";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "post116";

    public static void main(String[] args) {

        //время начала игры
        long startTimeMills = System.currentTimeMillis();

        //обьект занимающийся подключениями - реализован в методе getConnection в классе CustomDataSource
        //DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);

        //подключение с помощью Hikari Connection - пул соединений с базой данных
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(JDBC_URL);
        config.setDriverClassName("org.postgresql.Driver");
        config.setUsername(JDBC_USER);
        config.setPassword(JDBC_PASSWORD);
        //установить количество потоков подключений
        config.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(config);

        //JDBC repository
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);

        //file repository
//        PlayersRepository playersRepository = new PlayersRepositoryFileImpl(PLAYER_DB_TXT, PLAYER_SEQUENCE_TXT);
//        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl(SHOTS_DB_TXT, SHOTS_SEQUENCE_TXT);

        //list and map repository
//        GamesRepository gamesRepository = new GamesRepositoryListImpl();
//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
//        ShotsRepository shotsRepository = new ShotsRepositoryListImpl();

        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        //нижеследующий код должен выполниться 3 раза
        for (int j = 0; j < 3; j++) {
            System.out.println("Enter Player One name:");
            Scanner scanner = new Scanner(System.in);
            String playerOne = scanner.nextLine();
            System.out.println("Enter Player Two name:");
            String playerTwo = scanner.nextLine();
            Random random = new Random();

            //запуск игры
            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", playerOne, playerTwo);
            String playerWithGun = playerOne;
            String playerTarget = playerTwo;

            for (int i = 0; i < 10; i++) {
                System.out.println("Player '" + playerWithGun + "' take your weapon and shoot Player '" + playerTarget + "'");
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println(ANSI_GREEN + "Success! Nice shoot!" + ANSI_RESET);
                    gameService.shot(gameId, playerWithGun, playerTarget);
                } else {
                    System.out.println(ANSI_YELLOW + "Ha-Ha! You miss!" + ANSI_RESET);
                }
                String temp = playerWithGun;
                playerWithGun = playerTarget;
                playerTarget = temp;
            }
            //вывод информации о игре
            StatisticDto statistic = gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
            System.out.println(statistic);
        }

    }

}
