package ru.innopolis.stc37.udalov.project.repository;

import ru.innopolis.stc37.udalov.project.models.Game;
import ru.innopolis.stc37.udalov.project.models.Player;
import ru.innopolis.stc37.udalov.project.utils.JdbcUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "INSERT INTO " +
            "game (date_of_game, player_one, player_two, player_one_shot_count, player_two_shot_count, time_of_game) VALUES (?, ?, ?, ?, ?, ?);";
    //language=SQL
    private static final String SQL_FIND_BY_PLAYER_ID = "SELECT * FROM player WHERE id = ?";
    //language=SQL
    private static final String SQL_FIND_BY_GAME_ID = "SELECT * FROM game WHERE id = ?";
    //language=SQL
    private static final String SQL_UPDATE_GAME_BY_ID = "UPDATE game SET " +
            "date_of_game = ?, player_one = ?, player_two = ?, player_one_shot_count = ?, player_two_shot_count = ?, time_of_game = ? WHERE id = ?";

    //ссылка на метод, которая позволяет преобразовать строку из ResultSet в объект типа Game
    private static final RowMapper<Game> gameRowMapper = JdbcUtil::mapGameRow;

    //ссылка на метод, которая позволяет преобразовать строку из ResultSet в объект типа Player
    private static final RowMapper<Player> playerRowMapper = JdbcUtil::mapPlayerRow;

    //зависимость
    private final DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {

        try (Connection connection = dataSource.getConnection();
             //RETURN_GENERATED_KEYS - запрос должен вернуть ключи(id), которые сгенерировала БД
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getDateOfGame().toString());
            statement.setLong(2, game.getPlayerOne().getId());
            statement.setLong(3, game.getPlayerTwo().getId());
            statement.setInt(4, game.getPlayerOneShootsCounters());
            statement.setInt(5, game.getPlayerTwoShootsCounters());
            statement.setLong(6, game.getTimeOfGame());

            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();
            //если affectedRows не равен 1, то не получилось добавить игрока
            if (affectedRows != 1) {
                throw new SQLException("Can't insert game!");
            }
            //создание id для новой игры
            //ключи которые база сгенерировала сама для этого запроса
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                //проверяем сгенерировала ли база что либо?
                if (generatedId.next()) {
                    //получаем ключ, который сгенерировала база для текущего запроса
                    game.setId(generatedId.getLong("id")); //получаем id который сгенерировала база
                } else {
                    throw new SQLException("Can't retrieve id!"); //если id не сгенерирован
                }

            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_GAME_ID)) {
            //кладем gameId в данный statement
            statement.setLong(1, gameId);
            // получаем объект типа Game с полями типа Player, у которых есть только id
            Game game = null;
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    game = gameRowMapper.mapRow(rows);
                }
            }
            // если объект типа Game существует, запросим по id игроков недостающие данные об этих игроках
            if (game != null) {
                Player firstPlayer = null;
                try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_BY_PLAYER_ID)) {
                    ps.setLong(1, game.getPlayerOne().getId());
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            firstPlayer = playerRowMapper.mapRow(rs);
                        }
                    }
                }
                Player secondPlayer = null;
                try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_BY_PLAYER_ID)) {
                    ps.setLong(1, game.getPlayerTwo().getId());
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            secondPlayer = playerRowMapper.mapRow(rs);
                        }
                    }
                }
                // дозаполняем объект типа Game сформированными игроками
                game.setPlayerOne(firstPlayer);
                game.setPlayerTwo(secondPlayer);
            }
            return game;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID)) {
            statement.setString(1, game.getDateOfGame().toString());
            statement.setLong(2, game.getPlayerOne().getId());
            statement.setLong(3, game.getPlayerTwo().getId());
            statement.setInt(4, game.getPlayerOneShootsCounters());
            statement.setInt(5, game.getPlayerTwoShootsCounters());
            statement.setLong(6, game.getTimeOfGame());
            statement.setLong(7, game.getId());
            //сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
