package ru.innopolis.stc37.udalov.homework14;

import java.util.List;
import java.util.Optional;

public interface CarRepository {

    /**
     * Разделение строки из файла через счимвол # на отдельные строки
     *
     * @return список авто
     */
    List<Car> carsToLines();


    /**
     * Находим номера авто имеющих черный цвет или 0 км пробег
     *
     * @param color   - цвет авто
     * @param mileage - пробег авто
     * @return список номеров искомых авто
     */
    List<String> findBlackCar(String color, Long mileage);

    /**
     * Находим количество уникальных моделей авто в указанном диапазоне
     *
     * @param lowPrice  нижнее значение цены
     * @param highPrice верхнее значение цены
     * @return количество уникальных моделей авто
     */
    List<Integer> findUniqueModelsCarByPrice(Long lowPrice, Long highPrice);

    /**
     * Находим цвет авто с минимальной стоимостью
     *
     * @return цвет авто
     */
    Optional<String> colorCarByMinPrice();

    /**
     * Средняя стоимость на авто указанной марки (Доп. задание)
     *
     * @param model - модель авто
     * @return среднее значение стоимости
     */
    List<Double> carCamryAveragePrice(String model);
}
