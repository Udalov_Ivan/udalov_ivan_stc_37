package ru.innopolis.stc37.udalov.homework14;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;


public class CarRepositoryFileImpl implements CarRepository {

    private String fileName;

    public CarRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> carsToLines() {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Car> cars = reader
                    .lines()
                    .map(getStringCarFile())
                    .collect(Collectors.toList());
            reader.close();
            return cars;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<String> findBlackCar(String color, Long mileage) {

        return carsToLines().stream()
                .filter(car -> car.getCarColor().equals(color) || car.getCarMileage().equals(mileage))
                .distinct()
                .map(Car::getCarNumber)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> findUniqueModelsCarByPrice(Long lowPrice, Long highPrice) {

        return Collections.singletonList((int) carsToLines().stream()
                .filter(car -> car.getCarPrice() > lowPrice && car.getCarPrice() < highPrice)
                .map(Car::getCarModel)
                .distinct()
                .count());
    }

    @Override
    public Optional<String> colorCarByMinPrice() {
        return carsToLines().stream()
                .min(Comparator.comparing(Car::getCarPrice))
                .map(Car::getCarColor);
    }

    @Override
    public List<Double> carCamryAveragePrice(String model) {
        return carsToLines().stream()
                .filter(cars -> cars.getCarModel().equals(model))
                //.flatMap(cars -> cars.getCarPrice().stream())
                .map(cars -> {
                    double arithmeticMean = 0;
                    double sum = 0;
                    for (int i = 0; i < carsToLines().size(); i++) {
                        sum += cars.getCarPrice();
                        arithmeticMean = sum / carsToLines().size();
                    }
                    return arithmeticMean;

                })
                .collect(Collectors.toList());
        //System.out.println(carRepository.carCamryAveragePrice("Camry").size());

    }

    private Function<String, Car> getStringCarFile() {
        return line -> {
            String[] parsedLine = line.split("#");
            return new Car(parsedLine[0],
                    parsedLine[1],
                    parsedLine[2],
                    Long.parseLong(parsedLine[3]),
                    Long.parseLong(parsedLine[4]));
        };
    }
}
