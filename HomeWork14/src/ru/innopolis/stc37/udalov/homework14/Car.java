package ru.innopolis.stc37.udalov.homework14;

import java.util.Objects;
import java.util.StringJoiner;

public class Car {
    private String carNumber;
    private String carModel;
    private String carColor;
    private Long carMileage;
    private Long carPrice;

    public Car(String carNumber, String carModel, String carColor, Long carMileage, Long carPrice) {
        this.carNumber = carNumber;
        this.carModel = carModel;
        this.carColor = carColor;
        this.carMileage = carMileage;
        this.carPrice = carPrice;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public Long getCarMileage() {
        return carMileage;
    }

    public void setCarMileage(Long carMileage) {
        this.carMileage = carMileage;
    }

    public Long getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(Long carPrice) {
        this.carPrice = carPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(carNumber, car.carNumber) && Objects.equals(carModel, car.carModel) && Objects.equals(carColor, car.carColor) && Objects.equals(carMileage, car.carMileage) && Objects.equals(carPrice, car.carPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carNumber, carModel, carColor, carMileage, carPrice);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("carNumber='" + carNumber + "'")
                .add("carModel='" + carModel + "'")
                .add("carColor='" + carColor + "'")
                .add("carMileage=" + carMileage)
                .add("carPrice=" + carPrice)
                .toString();
    }
}
