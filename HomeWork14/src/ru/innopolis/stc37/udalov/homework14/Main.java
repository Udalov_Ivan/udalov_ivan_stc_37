package ru.innopolis.stc37.udalov.homework14;

public class Main {

    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryFileImpl("car_db.txt");

        //исходный список авто
        System.out.println(carRepository.carsToLines());
        System.out.println();

        //список номеров авто черного цвета или имеющих 0 км пробег;
        System.out.println("List of license plates for black color cars or cars with 0 km mileage:");
        System.out.println(carRepository.findBlackCar("Black", 0L));
        System.out.println();

        //количество уникальных моделей авто в диапазоне цен от 700000 до 800000
        System.out.println("Number of unique cars in the price range from 700,000 to 800,000:");
        System.out.println(carRepository.findUniqueModelsCarByPrice(700000L, 800000L));
        System.out.println();

        //цвет авто с минимальной стоимостью
        System.out.println("Color at the lowest car price:");
        System.out.println(carRepository.colorCarByMinPrice());
        System.out.println();

        //Средняя стоимость Camry(Доп.задание)
        System.out.println("Camry average price:");
        System.out.println(carRepository.carCamryAveragePrice("Camry"));
       // System.out.println(carRepository.carsToLines().size());
    }
}
