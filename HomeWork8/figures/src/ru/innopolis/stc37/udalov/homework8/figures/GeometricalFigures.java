package ru.innopolis.stc37.udalov.homework8.figures;

import ru.innopolis.stc37.udalov.homework8.change.ChangeCentre;
import ru.innopolis.stc37.udalov.homework8.change.ChangeScale;

public abstract class GeometricalFigures implements ChangeCentre, ChangeScale {
    //класс предка фигур, имплементирует интерфейс ru.innopolis.stc37.udalov.homework8.change.ChangeCentre, ru.innopolis.stc37.udalov.homework8.change.ChangeScale

    private static final double PI = 3.14;
    private static final int CENTRE_X = 0;
    private static final int CENTRE_Y = 0;
    private static final int OA = 1;
    private static final int TWO = 2;

    //центр фигур Х и Y
    protected int centreX = CENTRE_X;
    protected int centreY = CENTRE_Y;

    protected double scaleChange = 0;
    protected int changeX = 0;
    protected int changeY = 0;
    protected int oa = OA;//расстояние от центра О до точки А
    protected double areaS;
    protected double perimetrP;
    protected double pi = PI;
    protected int two = TWO;

    //конструктор предка
    public GeometricalFigures(int oa, int centreX, int centreY) {
        this.oa = oa;
        this.centreX = centreX;
        this.centreY = centreY;
    }

    //метод вывода информации о фигуре
    public abstract void info();

    //метод расчета площади фигуры
    public abstract double area();

    //метод расчета периметра фигуры
    public abstract double perimetr();

    @Override
    public int centreChangedX(int a) {
        return changeX = a + centreX;
    }

    @Override
    public int centreChangedY(int b) {
        return changeY = b + centreY;
    }

    @Override
    public double ChangeScale(int c) {
        scaleChange = Math.abs(areaS * c * c);
        return scaleChange;

    }

    @Override
    public void printInfo() {
        System.out.println("Text");
    }

}
