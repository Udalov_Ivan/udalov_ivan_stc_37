package ru.innopolis.stc37.udalov.homework8.figures;

public class Rectangle extends Square {
    //ru.innopolis.stc37.udalov.homework8.figures.Rectangle наследует класс ru.innopolis.stc37.udalov.homework8.figures.Square

    //расстояние от точки А до точки B
    private int ab;

    public Rectangle(int oa, int centreX, int centreY, int ab) {
        super(oa, centreX, centreY);
        this.ab = ab;
    }

    @Override
    public void info() {
        System.out.println("Rectangle info: ");
        System.out.println("The center is: X - " + centreX + "; Y - " + centreY + ";");
        System.out.println("The distance from the center O of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle to point A is: " + oa + ";");
        System.out.println("The distance from the point A of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle to point B is: " + ab + ";");
        System.out.println("Base area of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle: " + area() + ";");
        System.out.println("Base perimetr of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle: " + perimetr() + ";");
        System.out.println();
    }

    @Override
    public double area() {
        double oa2 = oa * two;
        areaS = ab * Math.sqrt(oa2 * oa2 - ab);
        return oa2;
    }

    @Override
    public double perimetr() {
        perimetrP = two * (ab + areaS / ab);
        return perimetrP;
    }

    @Override
    public void printInfo() {
        System.out.println("New Rectangle info: ");
        System.out.println("Centre X of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle changed: " + changeX + ";");
        System.out.println("Centre Y of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle changed: " + changeY + ";");
        System.out.println("Scale of the ru.innopolis.stc37.udalov.homework8.figures.Rectangle changed to: " + scaleChange + ";");
        System.out.println();
    }

}
