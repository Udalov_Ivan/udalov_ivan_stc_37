package ru.innopolis.stc37.udalov.homework8;

import ru.innopolis.stc37.udalov.homework8.figures.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //создание фигур и вывод информации о них:
        //круг
        GeometricalFigures circle = new Circle(3, 1, 2);
        //элипс
        GeometricalFigures ellipse = new Ellipse(2, 1, 0, 1);
        //квадрат
        GeometricalFigures square = new Square(2, 0, 1);
        //прямоугольник
        GeometricalFigures rectangle = new Rectangle(3, 0, 0, 2);

        //массив фигур
        GeometricalFigures[] figures = {circle, ellipse, square, rectangle};

        //информация о фигурах
        for (int i = 0; i < figures.length; i++) {
            figures[i].info();
        }

        //изменить координанту центра Х
        System.out.println("Input number to ru.innopolis.stc37.udalov.homework8.change centre X of the ru.innopolis.stc37.udalov.homework8.figures: ");
        int a = scanner.nextInt();

        //изменить координанту центра Y
        System.out.println("Input number to ru.innopolis.stc37.udalov.homework8.change centre Y of the ru.innopolis.stc37.udalov.homework8.figures: ");
        int b = scanner.nextInt();

        //изменить размер площади фигур
        System.out.println("Input number to ru.innopolis.stc37.udalov.homework8.change scale of the ru.innopolis.stc37.udalov.homework8.figures: ");
        int c = scanner.nextInt();

        //изменение координат и размера площади фигур, а так же новая информация о фигурах
        for (int i = 0; i < figures.length; i++) {
            figures[i].centreChangedX(a);
            figures[i].centreChangedY(b);
            figures[i].ChangeScale(c);
            figures[i].printInfo();
        }

    }
}
