package ru.innopolis.stc37.udalov.homework8.figures;

public class Square extends GeometricalFigures {
    //ru.innopolis.stc37.udalov.homework8.figures.Square наследует класс ru.innopolis.stc37.udalov.homework8.figures.GeometricalFigures

    public Square(int oa, int centreX, int centreY) {
        super(oa, centreX, centreY);
    }

    @Override
    public void info() {
        System.out.println("Square info: ");
        System.out.println("The center is: X - " + centreX + "; Y - " + centreY + ";");
        System.out.println("The distance from the center O of the ru.innopolis.stc37.udalov.homework8.figures.Square to point A is: " + oa + ";");
        System.out.println("Base area of the ru.innopolis.stc37.udalov.homework8.figures.Square: " + area() + ";");
        System.out.println("Base perimetr of the ru.innopolis.stc37.udalov.homework8.figures.Square: " + perimetr() + ";");
        System.out.println();
    }

    //площадь
    @Override
    public double area() {
        double oa2 = oa * two;
        areaS = (oa2 * oa2) / two;
        return oa2;
    }

    //периметр
    @Override
    public double perimetr() {
        perimetrP = 2 * Math.sqrt(4 * oa);
        return perimetrP;
    }

    @Override
    public void printInfo() {
        System.out.println("New Square info: ");
        System.out.println("Centre X of the ru.innopolis.stc37.udalov.homework8.figures.Square changed: " + changeX + ";");
        System.out.println("Centre Y of the ru.innopolis.stc37.udalov.homework8.figures.Square changed: " + changeY + ";");
        System.out.println("Scale of the ru.innopolis.stc37.udalov.homework8.figures.Square changed to: " + scaleChange + ";");
        System.out.println();
    }

}
