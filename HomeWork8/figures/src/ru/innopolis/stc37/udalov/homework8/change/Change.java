package ru.innopolis.stc37.udalov.homework8.change;

@FunctionalInterface
public interface Change {
    //интерфейс с методом печати
    void printInfo();
}
