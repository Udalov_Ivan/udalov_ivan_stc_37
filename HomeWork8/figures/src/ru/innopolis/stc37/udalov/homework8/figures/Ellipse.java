package ru.innopolis.stc37.udalov.homework8.figures;

public class Ellipse extends Circle  {
    //ru.innopolis.stc37.udalov.homework8.figures.Ellipse наследует класс ru.innopolis.stc37.udalov.homework8.figures.Circle

    //расстояние от точки О до точки B
    private int ob;

    public Ellipse(int oa, int centreX, int centreY, int ob) {
        super(oa, centreX, centreY);
        this.ob = ob;
    }

    @Override
    public void info() {
        System.out.println("Ellipse info: ");
        System.out.println("The center is: X - " + centreX + "; Y - " + centreY + ";");
        System.out.println("The distance from the center O of the ru.innopolis.stc37.udalov.homework8.figures.Ellipse to point A is: " + oa + ";");
        System.out.println("The distance from the center O of the ru.innopolis.stc37.udalov.homework8.figures.Ellipse to point B is: " + ob + ";");
        System.out.println("Base area of a ru.innopolis.stc37.udalov.homework8.figures.Ellipse: " + area() + ";");
        System.out.println("Base perimetr of a ru.innopolis.stc37.udalov.homework8.figures.Ellipse: " + perimetr() + ";");
        System.out.println();
    }

    @Override
    public double area() {
        areaS = pi * oa * ob;
        return areaS;
    }

    @Override
    public double perimetr() {
        perimetrP = 4 * ((pi * oa * ob + (oa - ob)) / (oa + ob));
        return perimetrP;
    }

    @Override
    public void printInfo() {
        System.out.println("New Ellipse info: ");
        System.out.println("Centre X of a ru.innopolis.stc37.udalov.homework8.figures.Ellipse changed: " + changeX + ";" );
        System.out.println("Centre Y of a ru.innopolis.stc37.udalov.homework8.figures.Ellipse changed: " + changeY + ";");
        System.out.println("Scale of a ru.innopolis.stc37.udalov.homework8.figures.Ellipse changed to: " + scaleChange + ";");
        System.out.println();
    }

}
