package ru.innopolis.stc37.udalov.homework8.change;

public interface ChangeScale extends Change {
    //интерфейс наследующий интерфейс ru.innopolis.stc37.udalov.homework8.change.Change, добавляющий методы изменения площади фигур
    double ChangeScale(int b);
}
