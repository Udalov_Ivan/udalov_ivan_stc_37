package ru.innopolis.stc37.udalov.homework8.change;

public interface ChangeCentre extends Change {
    //интерфейс наследующий интерфейс ru.innopolis.stc37.udalov.homework8.change.Change, добавляющий методы изменения x и Y
    int centreChangedX(int a);

    int centreChangedY(int b);
}
