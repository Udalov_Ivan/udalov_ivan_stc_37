package ru.innopolis.stc37.udalov.homework8.figures;

public class Circle extends GeometricalFigures {
    //ru.innopolis.stc37.udalov.homework8.figures.Circle наследует класс ru.innopolis.stc37.udalov.homework8.figures.GeometricalFigures

    public Circle(int oa, int centreX, int centreY) {
        super(oa, centreX, centreY);
    }

    @Override
    public void info() {
        System.out.println("Circle info: ");
        System.out.println("The center is: X - " + centreX + "; Y - " + centreY + ";");
        System.out.println("The distance from the center O of the ru.innopolis.stc37.udalov.homework8.figures.Circle to point A is: " + oa + ";");
        System.out.println("Base area of the ru.innopolis.stc37.udalov.homework8.figures.Circle: " + area() + ";");
        System.out.println("Base perimetr of the ru.innopolis.stc37.udalov.homework8.figures.Circle: " + perimetr() + ";");
        System.out.println();
    }

    //площадь
    @Override
    public double area() {
        areaS = pi * oa * oa;
        return areaS;
    }

    //периметр
    @Override
    public double perimetr() {
        perimetrP = two * pi * oa;
        return perimetrP;
    }

    @Override
    public void printInfo() {
        System.out.println("New Circle info: ");
        System.out.println("Centre X of the ru.innopolis.stc37.udalov.homework8.figures.Circle changed: " + changeX + ";");
        System.out.println("Centre Y of the ru.innopolis.stc37.udalov.homework8.figures.Circle changed: " + changeY + ";");
        System.out.println("Scale of the ru.innopolis.stc37.udalov.homework8.figures.Circle changed to: " + scaleChange + ";");
        System.out.println();
    }
}
