package ru.innopolis.stc37.udalov.homework6;

public class RemoteController {

    private static Channel channel;

    public RemoteController(Channel channel) {
        RemoteController.channel = channel;
    }

    //использование пульта для выбора канала
    public static void useRemoteController(int useController) {
        switch (useController) {
            case (1):
                System.out.print("\"" + Channel.channelList(useController) + "\"" + " now show the program: ");
                System.out.println(Channel.programRand(1) + ";");
                break;
            case (2):
                System.out.print("\"" + Channel.channelList(useController) + "\"" + " now show the program: ");
                System.out.println(Channel.programRand(2) + ";");
                break;
            case (3):
                System.out.print("\"" + Channel.channelList(useController) + "\"" + " now show the program: ");
                System.out.println(Channel.programRand(3) + ";");
                break;
            case (4):
                System.out.print("\"" + Channel.channelList(useController) + "\"" + " now show the program: ");
                System.out.println(Channel.programRand(4) + ";");
                break;
            case (5):
                System.out.print("\"" + Channel.channelList(useController) + "\"" + " now show the program: ");
                System.out.println(Channel.programRand(5) + ";");
                break;
            default:
                System.err.println("<Channel not found> please choose channel from 1 to 5");
                break;
        }

    }

}
