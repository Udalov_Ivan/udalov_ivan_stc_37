package ru.innopolis.stc37.udalov.homework6;

import java.util.Random;

public class Channel {

    //массив каналов
    public static String channelList(int listOfChannels) {
        int realListOfChannels = listOfChannels - 1;
        String[] channelName = {
                "Channel-1",
                "Channel-2",
                "Channel-3",
                "Channel-4",
                "Channel-5"
        };
        return channelName[realListOfChannels];
    }

    //вывод случайной программы при выборе канала
    public static String programRand(int a) {
        Random rand = new Random();
        int randomProgram = rand.nextInt(10);

        if (a == 1) {
            return Program.programlList1(randomProgram);
        }
        if (a == 2) {
            return Program.programlList2(randomProgram);
        }
        if (a == 3) {
            return Program.programlList3(randomProgram);
        }
        if (a == 4) {
            return Program.programlList4(randomProgram);
        }
        if (a == 5) {
            return Program.programlList5(randomProgram);
        }
        return "";

    }

}






