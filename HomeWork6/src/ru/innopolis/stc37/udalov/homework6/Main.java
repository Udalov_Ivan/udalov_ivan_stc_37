package ru.innopolis.stc37.udalov.homework6;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //создаем новый ru.innopolis.stc37.udalov.homework6.TV
        TV tv = new TV("Common table TV", 20022021);

        //вывод информации о телевизоре
        int info = scanner.nextInt();
        tv.tvInfo(info);

        //выбор каналов со случайными программами
        int choose = scanner.nextInt();
        tv.chooseChannel(choose);

    }

}
