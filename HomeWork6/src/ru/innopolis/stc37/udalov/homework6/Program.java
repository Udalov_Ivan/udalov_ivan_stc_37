package ru.innopolis.stc37.udalov.homework6;

public class Program {

    //массив программ для каждого канала
    public static String programlList1(int listOfPrograms) {
        String[] programName = {
                "Please stand by_1",
                "Movies_1",
                "Sport_1",
                "Advertisement_1",
                "Animals_1",
                "History_1",
                "Discovery_1",
                "Ocean_1",
                "Weather_1",
                "News_1",
                "Animations_1"
        };
        return programName[listOfPrograms];
    }

    public static String programlList2(int listOfPrograms) {
        String[] programName = {
                "Please stand by_2",
                "Movies_2",
                "Sport_2",
                "Advertisement_2",
                "Animals_2",
                "History_2",
                "Discovery_2",
                "Ocean_2",
                "Weather_2",
                "News_2",
                "Animations_2"
        };
        return programName[listOfPrograms];
    }

    public static String programlList3(int listOfPrograms) {
        String[] programName = {
                "Please stand by_3",
                "Movies_3",
                "Sport_3",
                "Advertisement_3",
                "Animals_3",
                "History_3",
                "Discovery_3",
                "Ocean_3",
                "Weather_3",
                "News_3",
                "Animations_3"
        };
        return programName[listOfPrograms];
    }

    public static String programlList4(int listOfPrograms) {
        String[] programName = {
                "Please stand by_4",
                "Movies_4",
                "Sport_4",
                "Advertisement_4",
                "Animals_4",
                "History_4",
                "Discovery_4",
                "Ocean_4",
                "Weather_4",
                "News_4",
                "Animations_4"
        };
        return programName[listOfPrograms];
    }

    public static String programlList5(int listOfPrograms) {
        String[] programName = {
                "Please stand by_5",
                "Movies_5",
                "Sport_5",
                "Advertisement_5",
                "Animals_5",
                "History_5",
                "Discovery_5",
                "Ocean_5",
                "Weather_5",
                "News_5",
                "Animations_5"
        };
        return programName[listOfPrograms];
    }

}
