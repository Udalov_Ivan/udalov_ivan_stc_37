package ru.innopolis.stc37.udalov.homework6;

import java.util.Scanner;

public class TV {

    Scanner scanner = new Scanner(System.in);

    private static final String MODEL = "SAME_MODEL";
    private static final int PARTNUMBER = 101;

    private String model;
    private int partnumber;

    private static RemoteController remoteController;

    //конструктор ru.innopolis.stc37.udalov.homework6.TV
    public TV(String model, int partNumber) {
        this.model = model;
        this.partnumber = partNumber;
        System.out.println("Press \"0\" to turn on TV: ");
    }

    //конструктор для неизвестного ru.innopolis.stc37.udalov.homework6.TV
    public TV() {
        this.model = MODEL;
        this.partnumber = PARTNUMBER;
        System.out.println("Press \"0\" to turn on TV: ");
    }

    public String getModel() {
        return model;
    }

    public int getPartNumber() {
        return partnumber;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPartnumber(int partnumber) {
        this.partnumber = partnumber;
    }

    //вывод информации о ru.innopolis.stc37.udalov.homework6.TV
    public void tvInfo(int info) {
        //информация при включении ru.innopolis.stc37.udalov.homework6.TV
        if (info == 0) {
            System.out.println(
                    "<TV is on> \nInformation about TV: \n"
                            + "Model TV is: "
                            + "\""
                            + model
                            + "\""
                            + ";\n"
                            + "Part Number TV is: "
                            + partnumber
                            + ";\n"
                            + "Currently available channels: 1 - 5; \n"
            );
        } else {
            System.err.println("<rTV is turned off>");
            //или вывод информации, что телевизор выключен и завершение программы
            System.exit(0);
        }
        System.out.println("Select channel or press \"0\" to turn off TV: ");
    }

    //указать пульт для ru.innopolis.stc37.udalov.homework6.TV
    public void setRemoteController(RemoteController remoteController) {
        TV.remoteController = remoteController;
    }

    //выбор канала с помощью пульта
    public void switchChannel(int changeChannel) {
        RemoteController.useRemoteController(changeChannel);
    }

    //завершение работы ru.innopolis.stc37.udalov.homework6.TV или переход к выбору другого канала
    public void turnOffTv(int turnOff) {
        if (turnOff != 0) {
            switchChannel(turnOff);
        }
        // завершение программы при вводе "0"
        if (turnOff == 0) {
            System.exit(0);
        }

    }

    //выбор канала или выключение ru.innopolis.stc37.udalov.homework6.TV
    public void chooseChannel(int choose) {
        if (choose == 0) {
            System.err.println("<TV is off>");
        } else {
            //выбор каналов пока ru.innopolis.stc37.udalov.homework6.TV включен, при вводе 0 ru.innopolis.stc37.udalov.homework6.TV выключается
            while (choose != 0) {

                turnOffTv(choose);
                System.out.println();
                System.out.println("To turn off TV press \"0\" or choose next channel: ");
                choose = scanner.nextInt();

                if (choose == 0) {
                    System.err.println("<TV is off>");
                }

            }

        }

    }

}
