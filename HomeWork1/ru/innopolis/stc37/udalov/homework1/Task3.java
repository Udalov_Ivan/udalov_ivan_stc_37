package ru.innopolis.stc37.udalov.homework1;

import java.util.Scanner;
class Task3{
	
    public static void main(String[] args){
		          
        System.out.println("Enter numbers: ");
        
        Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
					
		int simpleMultip = 1;
		
		while (number != 0){
		    
			int diGitSum = diGitSum(number); 
			boolean isItSimple = isItSimple(diGitSum);
		    
			if (isItSimple){
			    
		        simpleMultip *= number;
		    }
		    number = scanner.nextInt();
		}
		
		if (simpleMultip > 1)
		System.out.println("Simple numbers multiplication: " + simpleMultip); 
		
		else 
        System.out.println("Simple numbers multiplication: Absent");
    }
     
    public static boolean isItSimple(int n) //простое ли число
    {
        boolean boolSimple = false;
        
        for (int i = 2; i <= Math.sqrt(n); i++){
            
            if (n % i == 0){
                
            boolSimple = true;
            break;
            }
        }

		if (!boolSimple)
		return true;

		else
		return false;
    }
    
    public static int diGitSum(int n){ //сумма цифр числа
    
		int digitSum = 0;
		
		while (n != 0){   
			digitSum = digitSum + n % 10;
			n = n / 10;
		}
		return digitSum;
    }     
}