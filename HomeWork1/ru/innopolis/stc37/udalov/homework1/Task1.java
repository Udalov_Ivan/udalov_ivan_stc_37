package ru.innopolis.stc37.udalov.homework1;

class Task1{
    
    public static void main(String[] args){
        int number = 12345;
        int digitsSum = 0;
		int BASE = 10;
        
        digitsSum += number % BASE;
        number = number / BASE; 
        digitsSum += number % BASE;
        number = number / BASE;
        digitsSum += number % BASE;
        number = number / BASE;
		digitsSum += number % BASE;
        number = number / BASE;
		digitsSum += number % BASE;
        number = number / BASE;
        
        System.out.println(digitsSum);
    }
}