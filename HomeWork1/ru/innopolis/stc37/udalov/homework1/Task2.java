package ru.innopolis.stc37.udalov.homework1;

class Task2{

     public static void main(String[] args){
        int number = 12345;       
        	
		int BASE = 2;
		
		long binaryNumber = number % BASE;
		number /= 2;
		binaryNumber += number % BASE * 10;
		number /= 2;
		binaryNumber += number % BASE * 100;
		number /= 2;		
		binaryNumber += number % BASE * 1_000;
		number /= 2;
		binaryNumber += number % BASE * 10_000;
		number /= 2;
		binaryNumber += number % BASE * 100_000;
		number /= 2;

		binaryNumber += number % BASE * 1_000_000;
		number /= 2;
		binaryNumber += number % BASE * 10_000_000;
		number /= 2;		
		binaryNumber += number % BASE * 100_000_000;
		number /= 2;
		binaryNumber += number % BASE * 1_000_000_000;
		number /= 2;
		binaryNumber += number % BASE * 10_000_000_000L;
		number /= 2;
		binaryNumber += number % BASE * 100_000_000_000L;
		number /= 2;
		binaryNumber += number % BASE * 1_000_000_000_000L;
		number /= 2;		
		binaryNumber += number % BASE * 10_000_000_000_000L;
		number /= 2;
		
		System.out.println(binaryNumber);
		
     }
}