package ru.udalov.game.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import ru.udalov.game.client.socket.SocketClient;
import ru.udalov.game.client.utils.GameUtils;

import java.net.URL;
import java.util.ResourceBundle;

//контроллер управления компонентами
public class MainController implements Initializable {

    private SocketClient socketClient;

    //утилитный клас отвечающий за перемещение игрока
    private GameUtils gameUtils;

    @FXML
    private Separator upperSeparator;

    @FXML
    private Separator lowerSeparator;

    @FXML
    private ImageView yellowTankDown;

    @FXML
    private ImageView yellowTankLeft;

    @FXML
    private ImageView yellowTankRight;

    @FXML
    private ImageView greenTankLeft;

    @FXML
    private ImageView greenTankRight;

    @FXML
    private ImageView greenTankDown;

    @FXML
    private ImageView expOne;

    @FXML
    private ImageView expTwo;

    @FXML
    private ImageView yellowTank;

    @FXML
    private ImageView greenTank;

    @FXML
    private ImageView hpPlayerImage;

    @FXML
    private ImageView hpTargetImage;

    @FXML
    private ImageView oOne;

    @FXML
    private ImageView oTwo;

    @FXML
    private ImageView oThree;

    @FXML
    private ImageView oFour;

    @FXML
    private ImageView tOne;

    @FXML
    private ImageView tTwo;

    @FXML
    private ImageView tThree;

    @FXML
    private ImageView tFour;

    @FXML
    private ImageView field;

    @FXML
    private Label gameFinished;

    @FXML
    private TextArea resultGame;

    @FXML
    private Separator separatorLeft;

    @FXML
    private Separator separatorRight;

    @FXML
    private Circle player;

    @FXML
    private Circle enemy;

    @FXML
    private Button buttonGo;

    @FXML
    private Button buttonConnection;

    @FXML
    private TextField textPlayerName;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label hpPlayer;

    @FXML
    private Label hpEnemy;

    //перехватчик событий (перехватывает нажатие на кнопку)
    private EventHandler<KeyEvent> keyEventEventHandler = event -> {
        //устанавливаем клавиши движения вправо (-> , D)
        if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.D) {
            rightTankVisible();
            gameUtils.goRight(player, greenTank, greenTankLeft, greenTankRight, greenTankDown, expOne);
            //отправка сообщени о направлении движения
            socketClient.sendMessage("right");
        }
        //устанавливаем клавиши движения влево (<- , A)
        else if (event.getCode() == KeyCode.LEFT || event.getCode() == KeyCode.A) {
            leftTankVisible();
            gameUtils.goLeft(player, greenTank, greenTankLeft, greenTankRight, greenTankDown, expOne);
            //отправка сообщени о направлении движения
            socketClient.sendMessage("left");
            //устанавливаем кнопку для выполнения выстрела
        } else if (event.getCode() == KeyCode.SPACE) {
            //создаем пулю на базе круга
            Circle bullet = gameUtils.createBulletFor(player, false);
            //при создании пули отправляем сообщение
            socketClient.sendMessage("shot");
            //устанавливаем клавишу ESC для выхода из игры
        } else if (event.getCode() == KeyCode.ESCAPE) {
            System.exit(0);
            //устанавливаем клавиши движения вверх (^ , W)
        } else if (event.getCode() == KeyCode.W || event.getCode() == KeyCode.UP) {
            upTankVisible();
            gameUtils.goUp(player, greenTank, greenTankLeft, greenTankRight, greenTankDown, expOne);
            //отправка сообщени о направлении движения пока игрок не пересек верхнюю границу
            if (!player.getBoundsInParent().intersects(upperSeparator.getBoundsInParent())) {
                socketClient.sendMessage("up");
            }
            //устанавливаем клавиши движения вниз (v , S)
        } else if (event.getCode() == KeyCode.S || event.getCode() == KeyCode.DOWN) {
            downTankVisible();
            gameUtils.goDown(player, greenTank, greenTankLeft, greenTankRight, greenTankDown, expOne);
            //отправка сообщени о направлении движения пока игрок не пересек нижнюю границу
            if (!player.getBoundsInParent().intersects(lowerSeparator.getBoundsInParent())) {
                socketClient.sendMessage("down");
            }
        }

    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        gameUtils = new GameUtils();

        //при нажатии на кнопку buttonConnection (Connection) - подключение к серверу
        buttonConnection.setOnAction(event -> {
            socketClient = new SocketClient(this, "localhost", 7777);
            //запуск run из socketClient
            new Thread(socketClient).start();
            //кнопка Connection становится неактивной при ее нажатии
            buttonConnection.setDisable(true);
            //кнопка и поле ввода теста становятся активными при подключении
            buttonGo.setDisable(false);
            textPlayerName.setDisable(false);
            gameUtils.setPane(pane);
            gameUtils.setClient(socketClient);
        });

        //при нажатии на кнопку buttonGo (Go) - отправка имени игрока на сервер
        buttonGo.setOnAction(event -> {
            //отправка сообщения из поля textPlayerName
            socketClient.sendMessage("name: " + textPlayerName.getText());
            //кнопка и поле ввода теста становятся неактивными после нажатия кнопки Go
            buttonGo.setDisable(true);
            textPlayerName.setDisable(true);
            //чтобы фокус не смещался в ввод текста
            buttonGo.getScene().getRoot().requestFocus();
        });

        gameUtils.setController(this);
    }

    private void downTankVisible() {
        greenTank.setVisible(false);
        greenTankRight.setVisible(false);
        greenTankLeft.setVisible(false);
        greenTankDown.setVisible(true);
    }

    private void upTankVisible() {
        greenTank.setVisible(true);
        greenTankRight.setVisible(false);
        greenTankLeft.setVisible(false);
        greenTankDown.setVisible(false);
    }

    private void leftTankVisible() {
        greenTank.setVisible(false);
        greenTankRight.setVisible(false);
        greenTankLeft.setVisible(true);
        greenTankDown.setVisible(false);
    }

    private void rightTankVisible() {
        greenTank.setVisible(false);
        greenTankRight.setVisible(true);
        greenTankLeft.setVisible(false);
        greenTankDown.setVisible(false);
    }

    public Separator getUpperSeparator() {
        return upperSeparator;
    }

    public Separator getLowerSeparator() {
        return lowerSeparator;
    }

    public ImageView getGreenTankLeft() {
        return greenTankLeft;
    }

    public ImageView getGreenTankRight() {
        return greenTankRight;
    }

    public ImageView getGreenTankDown() {
        return greenTankDown;
    }

    public ImageView getYellowTankDown() {
        return yellowTankDown;
    }

    public ImageView getYellowTankLeft() {
        return yellowTankLeft;
    }

    public ImageView getYellowTankRight() {
        return yellowTankRight;
    }

    public ImageView getExpOne() {
        return expOne;
    }

    public ImageView getExpTwo() {
        return expTwo;
    }

    public ImageView getYellowTank() {
        return yellowTank;
    }

    public ImageView getGreenTank() {
        return greenTank;
    }

    public ImageView getoOne() {
        return oOne;
    }

    public ImageView getoTwo() {
        return oTwo;
    }

    public ImageView getoThree() {
        return oThree;
    }

    public ImageView getoFour() {
        return oFour;
    }

    public ImageView gettOne() {
        return tOne;
    }

    public ImageView gettTwo() {
        return tTwo;
    }

    public ImageView gettThree() {
        return tThree;
    }

    public ImageView gettFour() {
        return tFour;
    }

    public ImageView getHpPlayerImage() {
        return hpPlayerImage;
    }

    public ImageView getHpTargetImage() {
        return hpTargetImage;
    }

    public Label getGameFinished() {
        return gameFinished;
    }

    public TextArea getResultGame() {
        return resultGame;
    }

    public Separator getSeparatorRight() {
        return separatorRight;
    }

    public Separator getSeparatorLeft() {
        return separatorLeft;
    }

    public Circle getEnemy() {
        return enemy;
    }

    public Circle getPlayer() {
        return player;
    }

    public Label getHpPlayer() {
        return hpPlayer;
    }

    public Label getHpEnemy() {
        return hpEnemy;
    }

    public GameUtils getGameUtils() {
        return gameUtils;
    }

    public EventHandler<KeyEvent> getKeyEventEventHandler() {
        return keyEventEventHandler;
    }
}