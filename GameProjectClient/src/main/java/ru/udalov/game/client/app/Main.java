package ru.udalov.game.client.app;

import com.sun.javafx.iio.ImageLoader;
import com.sun.javafx.iio.png.PNGImageLoaderFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import ru.udalov.game.client.controllers.MainController;
import ru.udalov.game.client.socket.SocketClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    private SocketClient socketClient;

    @Override
    public void start(Stage primaryStage) throws Exception {
        String fxmlFileName = "/fxml/Main.fxml";
        //загружаем .fxml файл с помощью loader
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFileName));

        //название окна
        primaryStage.setTitle("Game Project Client");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        //чтобы нельзя было менять размер окна
        primaryStage.setResizable(false);
        //получаем контроллер
        MainController controller = loader.getController();
        //берем обработчик нажатий из контроллера и добавляем его в сцену
        scene.setOnKeyPressed(controller.getKeyEventEventHandler());

        primaryStage.show();

    }
}
