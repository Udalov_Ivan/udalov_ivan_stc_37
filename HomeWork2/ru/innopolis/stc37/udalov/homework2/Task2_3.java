package ru.innopolis.stc37.udalov.homework2;

import java.util.Arrays;
import java.util.Scanner;
class Task2_3 {
    
    public static void main(String[] args) {
    System.out.println("Enter array size: ");
        
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    
    int array[] = new int[n];
    System.out.println("Enter array numbers: ");
    
		double sum = 0;
		double arithmeticMean = 0;
    
        for (int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
            sum += array[i];
            arithmeticMean = sum / n;
        }
        System.out.println("Array: \n" + Arrays.toString(array));             
        System.out.println("Arithmetic mean of array: \n" + arithmeticMean);
        
    }
}