package ru.innopolis.stc37.udalov.homework2;

import java.util.Arrays;
import java.util.Scanner;
class Task2_2 {
    
    public static void main(String[] args) {
    System.out.println("Enter array size: ");
        
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    
    int array[] = new int[n];
    System.out.println("Enter array numbers: ");
    
        for (int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println("Array: \n" + Arrays.toString(array));             
        
			for (int i = 0; i < n / 2; i++){
				int changePosition = array[n - i - 1];
				array[n - i - 1] = array[i];
				array[i] = changePosition;
			}
			System.out.println("Reversed Array: \n" + Arrays.toString(array));
    
    
    

    }
}