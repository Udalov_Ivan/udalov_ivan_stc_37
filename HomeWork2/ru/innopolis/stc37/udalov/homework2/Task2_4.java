package ru.innopolis.stc37.udalov.homework2;

import java.util.Arrays;
import java.util.Scanner;
class Task2_4 {
    
    public static void main(String[] args) {
    System.out.println("Enter array size: ");
        
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    
    int array[] = new int[n];
    System.out.println("Enter array numbers: ");

        for (int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println("Initial Array: \n" + Arrays.toString(array));
        
        int min = array[0];
        int positionOfMin = 0;
        int max = array[0];
        int positionOfMax = 0;
        
        for (int i = 1; i < n; i++){
                
            if (array[i] < min){
                min = array[i];
                positionOfMin = i;
            }
            
            if (array[i] > max){
                max = array[i];
                positionOfMax = i;
            }
        }
        
        int temp = array[positionOfMax];
        array[positionOfMax] = array[positionOfMin];
        array[positionOfMin] = temp;
        
        
        System.out.println("Change position Max to Min from Array: \n" + Arrays.toString(array));
    }
}