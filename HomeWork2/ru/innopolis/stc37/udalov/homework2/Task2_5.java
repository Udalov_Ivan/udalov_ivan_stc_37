package ru.innopolis.stc37.udalov.homework2;

import java.util.Arrays;
import java.util.Scanner;
class Task2_5 {
    
    public static void main(String[] args) {
    System.out.println("Enter array size: ");
        
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    
    int array[] = new int[n];
    System.out.println("Enter array numbers: ");

        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println("Initial Array: \n" + Arrays.toString(array));
        
        boolean isArraySorted = false;
        int temp;
        
        while (!isArraySorted){
            isArraySorted = true;
            
            for (int j = 0; j < array.length - 1; j++){
                if (array[j] > array[j + 1]){
                    isArraySorted = false;
                    
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println("\"Bubble Sorted\" Array: \n" + Arrays.toString(array));
    }
}