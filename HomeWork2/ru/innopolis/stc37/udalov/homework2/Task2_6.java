package ru.innopolis.stc37.udalov.homework2;

import java.util.Arrays;
class Task2_6{

     public static void main(String[] args){
        int array[] = {4, 2, 3, 5, 7};
        int number = array[0];
		
        System.out.println("Initial Array: \n" + Arrays.toString(array));
        System.out.println();
        
        for (int i = 1; i < array.length; i++){
            number = number * 10 + array[i];
        }        
        System.out.println("Array to number: \n" + number); 
     }
}