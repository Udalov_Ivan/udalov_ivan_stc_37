package ru.innopolis.stc37.udalov.homework2;

class Task2_7{

     public static void main(String[] args) {
        
        int m = 3;
        int n = 3;
        int[][] array = new int[m][n];
		
		System.out.println("Spiral Array:");
        
        int spiral = 1;
		
		// заполнение внешнего витка массива
        for (int i = 0; i < n; i++) { 
            array[0][i] = spiral;
            spiral++;
        }
        for (int j = 1; j < m; j++) {
            array[j][n - 1] = spiral;
            spiral++;
        }
        for (int i = n - 2; i >= 0; i--) {
            array[m - 1][i] = spiral;
            spiral++;
        }
        for (int j = m - 2; j > 0; j--) {
            array[j][0] = spiral;
            spiral++;
        } 
		
        // заполнение следующих витков массива
        int a = 1;
        int b = 1;
		
		// заполнение массива вправо
        while (spiral < m * n) {
            while (array[a][b + 1] == 0) { 
                array[a][b] = spiral;
                spiral++;
                b++;
            }
			
            // заполнение массива вниз
            while (array[a + 1][b] == 0) { 
                array[a][b] = spiral;
                spiral++;
                a++;
            }
			
            // заполнение массива влево
            while (array[a][b - 1] == 0) { 
                array[a][b] = spiral;
                spiral++;
                b--;
            }
			
            // заполнение массива вверх
            while (array[a - 1][b] == 0) { 
                array[a][b] = spiral;
                spiral++;
                a--;
            }
        } 
		
        // заполнение последней центральной ячейки массива
        for (int j = 0; j < m; j++) { 
            for (int i = 0; i < n; i++) {
                if (array[j][i] == 0) {
                    array[j][i] = spiral;
                }
            }
        } 
		
        // вывод массива на печать
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                
                System.out.printf("%4d", array[j][i]);
            }
            System.out.println();
        }
    }
}