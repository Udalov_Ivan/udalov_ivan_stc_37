package ru.innopolis.stc37.udalov.homework7;

public class User {

    private String firstName = "unnamed";
    private String lastName = "unnamed";
    private int age;
    private boolean isWorker;

    //класс Builder
    protected static class Builder {

        private User user;

        public Builder(){
            user = new User();
        }


        //возврат значений по умолчанию
        public Builder firstName(String firstName) {
            user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            user.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            user.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            user.isWorker = isWorker;
            return this;
        }

        public User build() {
            return user;
        }

    }

    //метод печати информации о пользователе
    public String toString() {

        return "New user info: {" +
                "firstName = '" + firstName + '\'' +
                "; lastName = '" + lastName + '\'' +
                "; age = " + age +
                "; isWorker = " + isWorker +
                "}";
    }

}
