package ru.innopolis.stc37.udalov.homework7;

public class Main {

    public static void main(String[] args) {

        //инициализация пользователей через Builder
        User person1 = new User.Builder()
                .firstName("Name1")
                .lastName("LastName1")
                .age(18)
                .isWorker(true)
                .build();

        User person2 = new User.Builder()
                .firstName("Name2")
                .isWorker(false)
                .build();

        User person3 = new User.Builder()
                .firstName("Name3")
                .age(22)
                .build();

        User person4 = new User.Builder()
                .lastName("LastName4")
                .age(10)
                .build();

        User person5 = new User.Builder()
                .isWorker(true)
                .build();

        //вывод информации о пользователях из массива пользователей
        User[] users = {person1, person2, person3, person4, person5};

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i]);
        }

    }

}
