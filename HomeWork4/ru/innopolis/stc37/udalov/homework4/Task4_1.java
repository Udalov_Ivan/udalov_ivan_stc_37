package ru.innopolis.stc37.udalov.homework4;

import java.util.Scanner;

class Task4_1 {

    public static void powerOf2(int a) {

        if (a == 0 || a == 1){
            System.out.println("Yes, this number = " + a + " is a power of 2;");
        }

        else if (a % 2 != 0){
            System.out.println("No, this number = " + a + " is not a power of 2;");
        }

        else {
            powerOf2(a / 2);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number you want to check: ");
        int a = scanner.nextInt();

        powerOf2(a);

    }
}