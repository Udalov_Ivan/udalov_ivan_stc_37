package ru.innopolis.stc37.udalov.homework4;

import java.util.Arrays;
import java.util.Scanner;
class Task4_2{
    
    public static int binarySearch(int array[], int element) { //функция с 2 параметрами обращается к функции с 4 параметрами 
        if (element == 0){
		System.out.print("!!!ACHTUNG!!! Incorrect number: "); // в случае ввода 0 - выдается сообщение об ошибке, в противном случае выполняется поиск числа
		return 0;
		}
		else
		return binarySearch(array, 0, array.length - 1, element); 
    }

    public static int binarySearch(int array[], int left, int right, int element) { // функция бинарного поиска с помощью рекурсии
        
        int middle = (left + right) / 2;
        
        if(left > right) { 
            System.out.print("There is no such number in the array: ");
            return element;
        } 

        if(element == array[middle]) {
            System.out.print("Array contains this number: ");
            return element;
        } 
        
        else if(element < array[middle]) {
            return binarySearch(array, left, middle - 1, element);
        } 
        
        else {
            return binarySearch(array, middle + 1, right, element);
        }
        
    } 

    public static void main(String []args){
        
        System.out.println("Enter array size: ");
                
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];		
   	
        System.out.println("Enter array numbers: ");
        
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        
        System.out.println("Initial Array: \n" + Arrays.toString(array) + "\n");
            
        System.out.println("Enter the number you want to find: ");
        
        Scanner scanner1 = new Scanner(System.in);
        int element = scanner.nextInt();
        
        System.out.println();
       
        System.out.print(binarySearch(array, element));
    }
}