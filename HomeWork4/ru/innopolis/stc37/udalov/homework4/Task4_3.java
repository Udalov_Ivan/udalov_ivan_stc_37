package ru.innopolis.stc37.udalov.homework4;

import java.util.Arrays;
import java.util.Scanner;

public class Task4_3 {

    //для определения до n-1 числа Фибоначчи, при значении массива long[n]
    static long[] fibArray = new long[50];

    //вычисление числа Фибоначчи путем созранения значения в массив
    public static long fib(int n) {
        if (n <= 1)
            return n;

        long one;
        long two;

        if (fibArray[n - 1] != -1)
            one = fibArray[n - 1];
        else
            one = fib(n - 1);

        if (fibArray[n - 2] != -1)
            two = fibArray[n - 2];
        else
            two = fib(n - 2);

        return fibArray[n] = one + two;
    }

    public static void main(String[] args) {

        System.out.println("Enter Fibonacci Number: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Arrays.fill(fibArray, -1);

        System.out.println("Result is: " + fib(n) + ";");
    }
}